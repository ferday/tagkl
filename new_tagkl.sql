/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : new_tagkl

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-01-05 20:15:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES ('1', 'Foto Profil');

-- ----------------------------
-- Table structure for background
-- ----------------------------
DROP TABLE IF EXISTS `background`;
CREATE TABLE `background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `path` text NOT NULL,
  `full_path` text NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `owner` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of background
-- ----------------------------
INSERT INTO `background` VALUES ('1', 'pkl.jpg', '', 'assets/img/background/', 'assets/img/background/pkl.jpg', '31', '2016-10-01 23:14:05', 'Google');
INSERT INTO `background` VALUES ('2', '1024px-Pedagang_buah_pepaya_jambu_Jakarta.JPG', '', 'assets/img/background/', 'assets/img/background/1024px-Pedagang_buah_pepaya_jambu_Jakarta.JPG', '123', '2016-10-31 18:36:44', 'Wikipedia');
INSERT INTO `background` VALUES ('3', 'ananda_aktivitas-pedagang-ikan-subuh-di-pasar-bauntung-banjarbaru11.jpg', '', 'assets/img/background/', 'assets/img/background/ananda_aktivitas-pedagang-ikan-subuh-di-pasar-bauntung-banjarbaru11.jpg', '123', '2016-10-31 18:37:22', 'Google');
INSERT INTO `background` VALUES ('4', 'pedagang_kaki_lima_by_edwin1303-d5phdle.jpg', '', 'assets/img/background/', 'assets/img/background/pedagang_kaki_lima_by_edwin1303-d5phdle.jpg', '123', '2016-10-31 18:37:44', 'Google');
INSERT INTO `background` VALUES ('5', 'pkl2.jpg', '', 'assets/img/background/', 'assets/img/background/pkl2.jpg', '132', '2016-10-31 18:37:57', 'Google');

-- ----------------------------
-- Table structure for friendship
-- ----------------------------
DROP TABLE IF EXISTS `friendship`;
CREATE TABLE `friendship` (
  `user_one_id` int(11) unsigned NOT NULL,
  `user_two_id` int(11) unsigned NOT NULL,
  `status` int(1) NOT NULL,
  `action_user_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `unique_users_id` (`user_one_id`,`user_two_id`) USING BTREE,
  KEY `friendship_ibfk_2` (`user_two_id`),
  KEY `friendship_ibfk_3` (`action_user_id`),
  CONSTRAINT `friendship_ibfk_1` FOREIGN KEY (`user_one_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `friendship_ibfk_2` FOREIGN KEY (`user_two_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `friendship_ibfk_3` FOREIGN KEY (`action_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of friendship
-- ----------------------------

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'admin', 'Administrator');
INSERT INTO `groups` VALUES ('2', 'members', 'General User');
INSERT INTO `groups` VALUES ('3', 'pkl', 'PKL');

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `type` varchar(255) DEFAULT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `path` text,
  `full_path` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `album_id` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `default` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`),
  KEY `image_ibfk_1` (`user_id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `image_ibfk_2` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES ('23', '13096277_1717316451840458_7983496144568405784_n1.jpg', null, 'image/jpeg', null, '70.12', './assets/uploads/users/profile_picture/', './assets/uploads/users/profile_picture/13096277_1717316451840458_7983496144568405784_n1.jpg', '2016-12-29 18:58:50', null, null, '0');
INSERT INTO `image` VALUES ('24', '14716253_716744575142625_9025633034113160801_n.jpg', null, 'image/jpeg', null, '47.59', './assets/uploads/users/photo/', './assets/uploads/users/photo/14716253_716744575142625_9025633034113160801_n.jpg', '2016-12-29 19:00:03', null, null, '0');
INSERT INTO `image` VALUES ('25', '15337660_854324498043650_7613416423276576357_n1.jpg', null, 'image/jpeg', null, '27.02', './assets/uploads/pkl/product/', './assets/uploads/pkl/product/15337660_854324498043650_7613416423276576357_n1.jpg', '2016-12-29 19:03:06', null, null, '0');
INSERT INTO `image` VALUES ('28', '15401127_1186591108090764_5764303745945399364_n2.jpg', null, 'image/jpeg', '.jpg', '6.79', './assets/uploads/pkl/product/', './assets/uploads/pkl/product/15401127_1186591108090764_5764303745945399364_n2.jpg', '2017-01-01 11:05:51', null, '28', '0');
INSERT INTO `image` VALUES ('29', 'C360_2014-12-02-16-43-42-700.jpg', null, 'image/jpeg', '.jpg', '175.5', './assets/uploads/pkl/product/', './assets/uploads/pkl/product/C360_2014-12-02-16-43-42-700.jpg', '2017-01-01 11:13:45', null, '29', '0');
INSERT INTO `image` VALUES ('30', '1898244_738175632883901_283937982_n3.jpg', null, 'image/jpeg', '.jpg', '22.2', './assets/uploads/pkl/product/', './assets/uploads/pkl/product/1898244_738175632883901_283937982_n3.jpg', '2017-01-01 11:16:17', null, '30', '0');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for mailing_log
-- ----------------------------
DROP TABLE IF EXISTS `mailing_log`;
CREATE TABLE `mailing_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) DEFAULT NULL,
  `email_to` text,
  `cc` text,
  `bcc` text,
  `email_from` text,
  `alias` varchar(100) DEFAULT NULL,
  `body` text,
  `attachment` text,
  `filename` text,
  `status` int(1) DEFAULT '0',
  `error` text,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mailing_log
-- ----------------------------
INSERT INTO `mailing_log` VALUES ('41', 'TagKL Account Information', 'ferdi554@yahoo.co.id', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Dear Ferdi,<br /><br />\r\n		                Please click on the below activation link to verify your email address.<br /><br />\r\n		                http://[::1]/new_tagkl/action/verify/a260b2731c7e5d952cd4120b989b53e2<br /><br /><br />\r\n		                Thanks<br />\r\n		                Nuroaki', null, null, '0', null, '2016-12-29 06:58:19');
INSERT INTO `mailing_log` VALUES ('42', 'TagKL Account Information', 'cici@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 11:42:45');
INSERT INTO `mailing_log` VALUES ('43', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 11:43:17');
INSERT INTO `mailing_log` VALUES ('44', 'TagKL Account Information', 'asrulsujani@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 11:46:52');
INSERT INTO `mailing_log` VALUES ('45', 'TagKL Account Information', 'alanwar@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 12:01:21');
INSERT INTO `mailing_log` VALUES ('46', 'TagKL Account Information', 'alanwar@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 02:43:49');
INSERT INTO `mailing_log` VALUES ('47', 'TagKL Account Information', 'asrulsujani@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:45:28');
INSERT INTO `mailing_log` VALUES ('48', 'TagKL Account Information', 'alanwar@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:45:59');
INSERT INTO `mailing_log` VALUES ('49', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:46:58');
INSERT INTO `mailing_log` VALUES ('50', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:48:13');
INSERT INTO `mailing_log` VALUES ('51', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:48:49');
INSERT INTO `mailing_log` VALUES ('52', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 02:50:22');
INSERT INTO `mailing_log` VALUES ('53', 'TagKL Account Information', 'asrulsujani@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 02:50:25');
INSERT INTO `mailing_log` VALUES ('54', 'TagKL Account Information', 'alanwar@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-01 02:50:35');
INSERT INTO `mailing_log` VALUES ('55', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:50:43');
INSERT INTO `mailing_log` VALUES ('56', 'TagKL Account Information', 'asrulsujani@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-01 02:50:46');
INSERT INTO `mailing_log` VALUES ('57', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-02 11:01:01');
INSERT INTO `mailing_log` VALUES ('58', 'TagKL Account Information', 'asrulsujani@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-02 11:01:09');
INSERT INTO `mailing_log` VALUES ('59', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-02 11:15:11');
INSERT INTO `mailing_log` VALUES ('60', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-02 11:15:15');
INSERT INTO `mailing_log` VALUES ('61', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-02 11:17:14');
INSERT INTO `mailing_log` VALUES ('62', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-02 11:17:25');
INSERT INTO `mailing_log` VALUES ('63', 'TagKL Account Information', 'alanwar@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-02 11:50:57');
INSERT INTO `mailing_log` VALUES ('64', 'TagKL Account Information', 'alanwar@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-02 12:04:51');
INSERT INTO `mailing_log` VALUES ('65', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Mohon maaf', null, null, '0', null, '2017-01-03 07:55:24');
INSERT INTO `mailing_log` VALUES ('66', 'TagKL Account Information', 'alnisap@gmail.com', null, null, 'ferday554@gmail.com', 'TagKL Team', 'Selamat!', null, null, '0', null, '2017-01-03 07:55:30');

-- ----------------------------
-- Table structure for pkl
-- ----------------------------
DROP TABLE IF EXISTS `pkl`;
CREATE TABLE `pkl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pkl_ibfk_1` (`user_id`),
  CONSTRAINT `pkl_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pkl
-- ----------------------------
INSERT INTO `pkl` VALUES ('36', '28', 'Pulsa Alan', '1');
INSERT INTO `pkl` VALUES ('37', '29', 'Bacang Asrul', '1');
INSERT INTO `pkl` VALUES ('38', '30', 'Pulsa Alnisa', '1');

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status_ibfk_1` (`user_id`),
  CONSTRAINT `status_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=443 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status
-- ----------------------------
INSERT INTO `status` VALUES ('442', 'Status Pertama', '2016-12-29 19:00:03', '24');

-- ----------------------------
-- Table structure for status_image
-- ----------------------------
DROP TABLE IF EXISTS `status_image`;
CREATE TABLE `status_image` (
  `image_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  KEY `status_image_ibfk_2` (`status_id`),
  KEY `status_image_ibfk_1` (`image_id`),
  CONSTRAINT `status_image_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`) ON DELETE CASCADE,
  CONSTRAINT `status_image_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_image
-- ----------------------------
INSERT INTO `status_image` VALUES ('24', '442');

-- ----------------------------
-- Table structure for status_like
-- ----------------------------
DROP TABLE IF EXISTS `status_like`;
CREATE TABLE `status_like` (
  `status_id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`user_id`),
  KEY `status_like_ibfk_2` (`user_id`),
  CONSTRAINT `status_like_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE,
  CONSTRAINT `status_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_like
-- ----------------------------
INSERT INTO `status_like` VALUES ('442', '24');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `profile_picture` int(11) DEFAULT NULL,
  `cover_picture` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `postal_code` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cover_picture` (`cover_picture`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '127.0.0.1', 'admin', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', null, null, null, '1268889823', '1483618883', '1', 'Admin', 'TagKL', '1', null, null, null, null);
INSERT INTO `users` VALUES ('24', '::1', 'ferdi554', '$2y$08$Ln54F1y0ta861/9wOE.7uOgS3/dqCbPU84JvFvuWzcY4nzOPbz/Du', null, 'ferdi554@yahoo.co.id', null, null, null, null, '1483012699', '1483621108', '1', 'Ferdi', 'Ferdiansyah', '0', '23', null, null, null);
INSERT INTO `users` VALUES ('28', '::1', 'alan', '$2y$08$iTgG.uWeK6bnZ0glrwnSDeqVwGg5D2RyDsLtG7CKmlT5TyNaOWATy', null, 'alanwar@gmail.com', null, null, null, null, '1483243551', null, '1', 'Alan', 'Wars', '1', '28', null, 'Cipatat', '40553');
INSERT INTO `users` VALUES ('29', '::1', 'asrul', '$2y$08$LmulP95gayH3ZsTfBZ88M./0kMKHXPAkUkzX5uP4IvGJgcF3OLD.u', null, 'asrulsujani@gmail.com', null, null, null, null, '1483244025', '1483621183', '1', 'Asrul', 'Sujana', '0', '29', null, 'Cibacang', '40443');
INSERT INTO `users` VALUES ('30', '::1', 'alnissa', '$2y$08$COBBRpP9qBm7owvY7mNYt.6teueEv2EmPCmEAdMxYml9/zwWoBfVS', null, 'alnisap@gmail.com', null, null, null, null, '1483244177', null, '1', 'Alnissa', 'Putri', '1', '30', null, 'Cipatat', '40553');
INSERT INTO `users` VALUES ('31', '::1', 'cici', '$2y$08$iheX5uIhh1N2K1XSiBJb2O3k6mvfsDdItyCsVGS0lY67XAqZMtN3e', null, 'cici@gmail.com', null, null, null, null, '1483244290', null, '1', 'Cici', 'Suminar', '0', null, null, 'Orion', '40553');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES ('1', '1', '1');
INSERT INTO `users_groups` VALUES ('2', '1', '2');
INSERT INTO `users_groups` VALUES ('28', '24', '2');
INSERT INTO `users_groups` VALUES ('32', '28', '3');
INSERT INTO `users_groups` VALUES ('33', '29', '3');
INSERT INTO `users_groups` VALUES ('34', '30', '3');
INSERT INTO `users_groups` VALUES ('35', '31', '3');

-- ----------------------------
-- Table structure for user_album
-- ----------------------------
DROP TABLE IF EXISTS `user_album`;
CREATE TABLE `user_album` (
  `album_id` int(11) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`album_id`,`user_id`),
  KEY `user_album_ibfk_2` (`user_id`),
  CONSTRAINT `user_album_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`),
  CONSTRAINT `user_album_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_album
-- ----------------------------
