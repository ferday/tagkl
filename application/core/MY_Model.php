<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	class MY_Model extends CI_Model {
        public function mailing_log($data = array()) {
            $default = array(
                'email_from' => 'ferday554@gmail.com',
                'alias' => 'TagKL Team',
                'date' => date('Y-m-d g:i:s')
            );
            if($data) {
                $this->db->insert('mailing_log', array_merge($default, $data));
            }
        }

		public function mail($to,$message) {
			$this->load->library('email');
			$from_email = 'ferday554@gmail.com'; // ganti dengan email kalian
		    $subject = 'Verify Your Email Address';
		    $config['protocol'] = 'smtp';
		    $config['smtp_host'] = 'ssl://smtp.gmail.com'; // sesuaikan dengan host email
		    $config['smtp_timeout'] = '7';
		    $config['smtp_port'] = '465'; // sesuaikan
		    $config['smtp_user'] = $from_email;
		    $config['smtp_pass'] = 'ferdismkn4padalarang'; // ganti dengan password email
		    $config['mailtype'] = 'html';
		    $config['charset'] = 'iso-8859-1';
		    $config['wordwrap'] = TRUE;
		    $config['newline'] = "\r\n";
		    $config['crlf'] = "\r\n";
		    $this->email->initialize($config);

		    $this->email->from($from_email, 'TagKL Team');
		    $this->email->to($to);
		    $this->email->subject($subject);
		    $this->email->message($message);
		    // gunakan return untuk mengembalikan nilai yang akan selanjutnya diproses ke verifikasi email
		    return $this->email->send();
		}

	}

?>