<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	class MY_Controller extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
            if(!$this->ion_auth->logged_in()) {
                $list = array('auth', 'auth/signup_modal/pembeli', 'auth/signup_modal/pkl', 'auth/login', 'auth/pkl_register', 'auth/register');
                if(!in_array(strtolower(uri_string()), $list)) {
                    redirect('auth');
                }
            }
		}
		
		public function _renderpage($view, $data = null, $json_format = FALSE) {
			return $this->load->view($view, $data, $json_format);
		}

		public function json_result($result) {
			header('Content-type: application/json');
			echo json_encode($result);
		}

		public function notification($name, $message) {
			return $this->session->set_flashdata($name, $message);
		}

        public function do_upload($name, $path, $allowed_type) {
            $config['upload_path']          = $path;
            $config['allowed_types']        = $allowed_type;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload($name)) {
                return $this->upload->data();
            } else {
                return false;
            }
        }

        public function mailing($id) {
            $detail = $this->db->get_where('mailing_log', array('id' => $id))->row_array();

            $this->load->library('email');
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'ssl://smtp.gmail.com';
            $config['smtp_timeout'] = '7';
            $config['smtp_port'] = '465';
            $config['smtp_user'] = $detail['email_from'];
            $config['smtp_pass'] = 'ferdismkn4padalarang';
            $config['mailtype'] = 'html';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['newline'] = "\r\n";
            $config['crlf'] = "\r\n";
            $this->email->initialize($config);

            $this->email->from($detail['email_from'], 'TagKL Team');
            $this->email->to($detail['email_to']);
            $this->email->subject($detail['subject']);
            $this->email->message($detail['body']);
            $send = $this->email->send();

            if($send) {
                $this->db->update('mailing_log', array('status' => 1), array('id' => $id));
            } else {
                $this->db->update('mailing_log', array('status' => 2), array('id' => $id));
            }

            return $id;
        }

        public function user_info($is_one = null) {
            if($is_one) {
                return $this->ion_auth->user()->row();
            } else {
                return $this->ion_auth->user();
            }
        }

        public function get_user_profile_picture($id) {
            $this->db->select('full_path');
            $this->db->where('id', $id);
            $image = $this->db->get('image')->row_array();

            return $image['full_path'];
        }

        public function _user($field = null) {
            $user_id = $this->ion_auth->user()->row()->id;
            $default = array('username', 'first_name', 'last_name', 'profile_picture');
            if($field) {
                if(is_array($field)) {
                    $fields = array_merge($default, $field);
                    $this->db->select(implode(',', $fields));
                } else {
                    $this->db->select($field);
                }
            } else {
                $this->db->select(implode(',', $default));
            }
            $this->db->where('id', $user_id);
            $user = $this->db->get('users')->row_array();

            if(array_key_exists('profile_picture', $user)) {
                if($user['profile_picture']) {
                    $user['profile_picture'] = $this->get_user_profile_picture($user['profile_picture']);
                } else {
                    $user['profile_picture'] = './assets/img/default-user.png';
                }
            }

            return $user;
        }

        public function on_duplicate($table, $data) {
            if (empty($table) || empty($data)) return false;
            $duplicate_data = array();
            foreach($data AS $key => $value) {
                $duplicate_data[] = sprintf("%s='%s'", $key, $value);
            }

            $sql = sprintf("%s ON DUPLICATE KEY UPDATE %s", $this->db->insert_string($table, $data), implode(',', $duplicate_data));
            $this->db->query($sql);
            return $this->db->insert_id();
        }

        public function _curl($url, $method = 'GET', $params = NULL, $return = 'data') {
            set_time_limit(0);
            $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_HEADER, false);
            switch ($method) {
                case "GET":

                    break;
                case "POST":
                    if ($params) {
                        curl_setopt($curl, CURLOPT_POST, TRUE);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
                    }
                    break;
                case "DELETE":
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                    break;
                case "JSON":
                    if ($params) {
                        curl_setopt($curl, CURLOPT_POST, TRUE);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen(json_encode($params))
                        ));
                    }
                    break;
            }
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT_MS, 120000);

            $data = json_decode(curl_exec($curl), TRUE);
            $http = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if (curl_error($curl)) {
                show_error(curl_error($curl), 500, "Error API");
            }

            curl_close($curl);

            switch ($return) {
                case 'http':
                    return $http;
                default:
                    return $data;
            }
        }
	}

?>