<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="mdc-text-grey-700" style="margin-bottom: 0;">Tulis Aktivitas</h4>
                <p class="mdc-text-grey-400">Tulis hal yang berguna bagi dagangan anda</p>
            </div>
            <div class="box-body">
                <form>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Tulis aktivitas..." style="height: 70px; border-radius: 0;"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info btn-block"><i class="zmdi zmdi-mail-send"></i> Terbitkan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>