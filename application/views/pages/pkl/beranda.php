<div class="row">
    <div class="col-md-3" style="margin-bottom: 5px;">
        <div class="box">
            <div class="box-header mdc-bg-blue">
                <h2 class="text-center mdc-text-white-darker" style="margin: 20px;">123</h2>
            </div>
            <div class="box-body">
                <h4 class="text-center mdc-text-grey-600">Total Pembeli Hari Ini</h4>
            </div>
        </div>
    </div>
    <div class="col-md-4" style="margin-bottom: 5px;">
        <div class="box">
            <div class="box-header mdc-bg-green-500">
                <h2 class="text-center mdc-text-white-darker" style="margin: 20px;">Rp 84.562,-</h2>
            </div>
            <div class="box-body">
                <h4 class="text-center mdc-text-grey-600">Pemasukkan Hari Ini</h4>
            </div>
        </div>
    </div>
    <div class="col-md-5" style="margin-bottom: 5px;">
        <div class="box">
            <div class="box-header mdc-bg-red-600">
                <h2 class="text-center mdc-text-white-darker" style="margin: 20px;">Rp 100.000,-</h2>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <h4 class="mdc-text-grey-600">Modal Hari Ini</h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <button class="btn btn-success pull-right"><i class="zmdi zmdi-money-box"></i> Hitung Laba</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body mdc-bg-blue-500">
                <h4 class="mdc-text-white-darker">
                    <i class="zmdi zmdi-my-location zmdi-hc-lg animated jello infinite"></i> Lokasi Anda Saat Ini
                </h4>
            </div>
            <div id="currentPositionMap" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <h3 class="mdc-text-grey-500">Navigasi</h3>
        <hr>
    </div>
</div>
