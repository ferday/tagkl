<div class="row">
    <div class="col-md-12">
        <div class="profile-box">
            <div class="cover">
                <img class="demo" src="<?php echo base_url('assets/img/background/1024px-Pedagang_buah_pepaya_jambu_Jakarta.JPG'); ?>" alt="Image" />
                <div class="name">
                    <center>
                        <img src="<?php echo base_url('assets/img/default-user.png'); ?>" class="img-circle profile-picture">
                    </center>
                    <h3>
                        <?php echo $pkl['product']; ?>
                    </h3>
                    <hr style="width: 50px;">
                    <p class="mdc-text-white-dark">
                        123 Pelanggan
                    </p>
                </div>
            </div>
            <div class="profile-box-body">
                <div class="row">
                    <div class="col-md-6">
                        <p style="margin: 10px;">
                            <i class="zmdi zmdi-navigation mdc-text-green-500"></i> Jam Buka: 08:00 - 18:00. <span class="mdc-text-green-500">Sedang Buka</span>
                        </p>
                    </div>
                    <div class="tab-container">
                        <div class="col-md-2" style="padding-right: 0;">
                            <div class="tab active" data-target="activity">
                                <span><i class="zmdi zmdi-graphic-eq"></i> &nbsp; AKTIVITAS</span>
                            </div>
                        </div>
                        <div class="col-md-2 no-padding">
                            <div class="tab" data-target="">
                                <span><i class="zmdi zmdi-comments"></i> &nbsp; ULASAN</span>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-left: 0;">
                            <div class="tab" data-target="">
                                <span><i class="zmdi zmdi-info-outline"></i> &nbsp; TENTANG</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-10">
        <div class="alert alert-info">
        	Ini adalah halaman persis seperti yang pelanggan anda lihat.
        </div>
    </div>
    <div class="col-md-2">
        <button class="btn btn-info btn-block"><i class="zmdi zmdi-edit"></i> Edit Profil</button>
    </div>
</div>
<div id="profile-content">

</div>