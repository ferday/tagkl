<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Add PKL</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <form id="form-create-pkl">
                <div class="box-body">
                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" class="form-control" name="product" placeholder="Product Name..." required>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="first_name" placeholder="First Name..." required>
                            </div>
                            <div class="col-md-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-10">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address" placeholder="Address..." required>
                            </div>
                            <div class="col-md-2">
                                <label>Postal Code</label>
                                <input type="text" class="form-control" name="postal_code" placeholder="Postal Code..." required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Email..." required>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-5">
                                <label>User Name</label>
                                <input type="text" class="form-control" name="username" placeholder="User Name..." required>
                            </div>
                            <div class="col-md-5">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password..." required>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <label style="font-weight: 500;">
                                    <input type="checkbox" name="generate">
                                    Generate automatically
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>