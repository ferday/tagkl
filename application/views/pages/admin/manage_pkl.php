<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue" id="pkl-request">
            <div class="inner">
                <h3><?php echo $data['total_request']; ?></h3>
                <p>New Request</p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
            </div>
            <a href="#" class="small-box-footer btn-change-content" data-content="pkl_request">
                Show <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green" id="pkl-accepted">
            <div class="inner">
                <h3><?php echo $data['total_accepted']; ?></h3>
                <p>Accepted PKL</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="#" class="small-box-footer btn-change-content" data-content="pkl_accepted">
                Show <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red" id="pkl-rejected">
            <div class="inner">
                <h3><?php echo $data['total_rejected']; ?></h3>
                <p>Rejected PKL</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-times"></i>
            </div>
            <a href="#" class="small-box-footer btn-change-content" data-content="pkl_rejected">
                Show <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>3</h3>
                <p>Active PKL</p>
            </div>
            <div class="icon">
                <i class="fa fa-universal-access"></i>
            </div>
            <a href="#" class="small-box-footer btn-change-content" data-content="pkl_active">
                Show <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>

<div id="manage-pkl-content"></div>
<div class="modal fade" id="pkl-detail-modal"></div>
<div class="modal fade" id="pkl-delete-modal"></div>
<div class="modal fade" id="pkl-block-modal"></div>
<div class="modal fade" id="pkl-edit-modal"></div>
<div class="modal fade" id="pkl-accept-modal"></div>