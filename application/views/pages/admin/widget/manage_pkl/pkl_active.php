<br>
<div class="row">
    <div class="col-md-8">
        <h3 class="no-margin">Active PKL</h3>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <input class="form-control" placeholder="Find Location...">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-body no-padding">
                <div id="active-pkl-map" style="width: 100%;height:500px;"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var map = new GMaps({
        el: '#active-pkl-map',
        lat: -12.043333,
        lng: -77.028333
    });
</script>