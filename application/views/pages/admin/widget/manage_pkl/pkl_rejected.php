<h3>Rejected PKL</h3>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body">
                <table class="table table-hover table-bordered" id="table-pkl_rejected">
                    <thead>
                    <th>No</th>
                    <th>Product Name</th>
                    <th>Owner</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                    <?php
                    if($rejected_list) {
                        $no = 1;
                        foreach($rejected_list as $v) {
                            ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $v['product']; ?></td>
                                <td><?php echo $v['first_name'].' '.$v['last_name']; ?></td>
                                <td><?php echo $v['address'].' - '.$v['postal_code']; ?></td>
                                <td><a href="mailto:<?php echo $v['email']; ?>"><?php echo $v['email']; ?></a></td>
                                <td class="text-center">
                                    <button class="btn btn-info btn-sm btn-detail-pkl" data-id="<?php echo $v['id']; ?>" data-source="pkl_rejected"><i class="fa fa-search"></i></button>
                                    <button class="btn btn-success btn-sm btn-accept-pkl" data-id="<?php echo $v['id']; ?>" data-name="<?php echo $v['first_name'].' '.$v['last_name']; ?>"><i class="fa fa-check"></i></button>
                                    <button class="btn btn-danger btn-sm btn-delete-pkl" data-id="<?php echo $v['id']; ?>" data-name="<?php echo $v['first_name'].' '.$v['last_name']; ?>" data-content="pkl_rejected"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $('#table-pkl_rejected').DataTable();
</script>