<h3 >New Request</h3>
<br>
<div class="row">
    <?php
    if($new_request) {
        foreach ($new_request as $v) {
            ?>
            <div class="col-md-3 pkl-list" data-id="<?php echo $v['id']; ?>">
                <div class="box box-primary">
                    <div class="box-body">
                        <h3 class="text-center no-margin" style="text-transform: capitalize;font-weight: 300;">
                            <?php echo $v['product']; ?>
                        </h3>
                        <h5 class="text-center text-muted">By <?php echo $v['first_name'].' '.$v['last_name']; ?></h5>
                        <?php
                        if($v['profile_picture']) {
                            ?>
                            <img src="<?php echo base_url() . $v['full_path']; ?>" class="img-responsive" alt="Product Image" style="width: 100%;">
                            <?php
                        }
                        ?>
                        <h5 class="text-center">In <?php echo $v['address'].' - '.$v['postal_code']; ?></h5>
                        <br>
                        <p class="text-center">Joined <?php echo timespan($v['created_on']); ?> ago</p>
                        <center>
                            <button class="btn btn-default request-action" data-id="<?php echo $v['id']; ?>" data-type="1"><i class="fa fa-check no-margin"></i> Accept</button>
                            <button class="btn btn-info btn-detail-pkl" data-id="<?php echo $v['id']; ?>" data-source="pkl_request">Detail</button>
                            <button class="btn btn-default request-action" data-id="<?php echo $v['id']; ?>" data-type="2"><i class="fa fa-remove no-margin"></i> Reject</button>
                        </center>
                    </div>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <h4 class="text-center">No Result</h4>
        <?php
    }
    ?>
</div>