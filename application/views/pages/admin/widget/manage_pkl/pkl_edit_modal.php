<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title" style="font-weight: 300;">Edit PKL</h3>
        </div>
        <form id="form-edit-pkl">
            <div class="modal-body">
                <div class="form-group">
                    <label>Product Name</label>
                    <input type="text" class="form-control" name="product" value="<?php echo $detail['product']; ?>">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" value="<?php echo $detail['first_name']; ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" value="<?php echo $detail['last_name']; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="pkl_id" value="<?php echo $detail['id']; ?>">
                <button type="submit" class="btn btn-success">Save</button>
                <a class="btn btn-default" data-dismiss="modal">Cancel</a>
            </div>
        </form>
    </div>
</div>