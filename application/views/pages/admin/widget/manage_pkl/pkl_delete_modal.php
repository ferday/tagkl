<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title" style="font-weight: 300;">Remove PKL</h3>
        </div>
        <form id="form-delete-pkl" data-content="<?php echo $content; ?>">
            <div class="modal-body">
                <p>
                    Are you sure want to remove <strong><?php echo $name; ?></strong> ?
                </p>
                <input type="hidden" name="pkl_id" value="<?php echo $pkl_id; ?>">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Remove</button>
                <a class="btn btn-default" data-dismiss="modal">Cancel</a>
            </div>
        </form>
    </div>
</div>