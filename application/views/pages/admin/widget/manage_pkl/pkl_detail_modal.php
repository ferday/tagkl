<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title" style="font-weight: 300;">PKL Detail</h3>
        </div>
        <div class="modal-body no-padding">
            <?php
                if($pkl_detail['profile_picture']) {
            ?>
            <img src="<?php echo base_url() . '/' . $pkl_detail['full_path']; ?>" style="min-width: 100%;" class="img-responsive">
            <?php
                }
            ?>
            <table class="table table-hover table-bordered">
                <tr>
                    <td class="col-md-4">Product Name</td>
                    <td><?php echo $pkl_detail['product']; ?></td>
                </tr>
                <tr>
                    <td>Owner Name</td>
                    <td><?php echo $pkl_detail['first_name'].' '.$pkl_detail['last_name']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><?php echo $pkl_detail['address'].' - '.$pkl_detail['postal_code']; ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?php echo $pkl_detail['email']; ?></td>
                </tr>
                <tr>
                    <td>Joined on</td>
                    <td><?php echo date('d M Y, H:i:s', $pkl_detail['created_on']); ?></td>
                </tr>
            </table>
            <?php
                if($source == 'pkl_request') {
                    ?>
                    <p class="text-center text-muted">
                        <button class="btn btn-default request-action" data-id="<?php echo $pkl_detail['id']; ?>"
                                data-type="1"><i class="fa fa-check no-margin"></i> Accept
                        </button>
                        &nbsp; OR &nbsp;
                        <button class="btn btn-default request-action" data-id="<?php echo $pkl_detail['id']; ?>"
                                data-type="2"><i class="fa fa-remove no-margin"></i> Reject
                        </button>
                    </p>
                    <br>
                    <?php
                }
            ?>
        </div>
    </div>
</div>