<div class="row">
    <div class="col-md-3">
        <div class="tagkl-box">
            <div class="header mdc-bg-red-500" style="padding: 20px;">
                <h3 class="no-margin text-center" style="line-height: 1.5;">
                    <i class="zmdi zmdi-settings zmdi-hc-lg zmdi-hc-spin"></i> <br> Pengaturan
                </h3>
            </div>
            <br>
            <ul class="nav nav-pills nav-stacked nav-setting">
                <li class="active"><a href="#" data-to="account">Akun</a></li>
                <li><a href="#" data-to="design">Desain Profil</a></li>
                <li><a href="#" data-to="security">Keamanan</a></li>
            </ul>
            <br>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tagkl-box" id="setting-container" style="min-height: 400px;padding:20px;"></div>
    </div>
</div>