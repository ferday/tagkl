<h3 class="no-margin mdc-text-grey-600">Pengaturan Desain Profil</h3>
<p class="mdc-text-grey-400">Atur foto profil dan gambar sampul disini</p>
<hr>
<div class="row">
    <div class="col-md-3">
        <p class="text-right">Foto Profil</p>
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-6">
                <div class="edit-profile-picture-container">
                    <img class="img-preview img-responsive" style="height: 100%; display: none;">
                    <h3 class="mdc-text-grey-500" style="line-height: 1.5; margin-top: 65px;">
                        <i class="zmdi zmdi-collection-image zmdi-hc-2x"></i>
                        <br>
                        Foto Profil
                    </h3>
                </div>
            </div>
            <div class="col-md-6">
                <form id="edit-design-profile-form">
                    <a class="fileUpload btn btn-danger btn-raised btn-block no-margin">
                        <span><i class="zmdi zmdi-upload"></i> Pilih Gambar</span>
                        <input name="profile_picture" type="file" class="upload" />
                    </a>
                    <br>
                    <ul class="mdc-text-grey-700" style="padding-left: 0;">
                        <li>Ukuran gambar tidak lebih dari 2 MB</li>
                        <li>Format gambar (.jpg, .png)</li>
                        <li>Mengikuti <a href="#">Syarat dan Ketentuan TagKL</a></li>
                    </ul>
                    <br>
                    <button type="submit" class="btn btn-danger btn-block no-margin">Simpan Foto Profil</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <p class="text-right">Gambar Sampul</p>
    </div>
    <div class="col-md-9">tes</div>
</div>