<h3 class="no-margin mdc-text-grey-600">Pengaturan Akun</h3>
<p class="mdc-text-grey-400">Atur nama dan nama pengguna disini</p>
<hr>
<form id="form-edit-account">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Nama Depan</label>
                <input type="text" name="first_name" class="form-control" value="<?php echo $user_data->first_name; ?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Nama Belakang</label>
                <input type="text" name="last_name" class="form-control" value="<?php echo $user_data->last_name; ?>">
            </div>
        </div>
    </div>
    <br>
    <div class="form-group">
        <label class="control-label">Nama Pengguna</label>
        <input type="text" name="username" class="form-control" value="<?php echo $user_data->username; ?>">
        <p class="mdc-text-blue-grey-300" style="font-size: 12px;"><span><?php echo base_url(); ?>user/</span><?php echo $user_data->username; ?></p>
    </div>
    <br>
    <div class="pull-right">
        <button type="submit" class="btn btn-danger"><i class="zmdi zmdi-check"></i> Simpan</button>
    </div>
</form>