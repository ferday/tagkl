<h3 class="no-margin mdc-text-grey-600">Pengaturan Keamanan</h3>
<p class="mdc-text-grey-400">Atur kata sandi disini</p>
<hr>
<form id="form-edit-security">
    <div class="form-group">
        <label class="control-label">Kata Sandi Aktif</label>
        <input type="password" name="current_password" class="form-control">
    </div>
    <br>
    <div class="form-group">
        <label class="control-label">Kata Sandi Baru</label>
        <input type="password" name="password" class="form-control">
    </div>
    <br>
    <div class="form-group">
        <label class="control-label">Konfirmasi Kata Sandi Baru</label>
        <input type="password" name="password_c" class="form-control">
    </div>
    <br>
    <div class="pull-right">
        <button type="submit" class="btn btn-danger"><i class="zmdi zmdi-check"></i> Simpan</button>
    </div>
    <br>
    <br>
</form>