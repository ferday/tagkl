<?php
    if($status) {
        foreach($status as $k => $v) {
            ?>
            <div class="tagkl-box news-feed-box animated fadeIn">
                <div class="news-feed-header">
                    <a href="#">
                        <div class="row">
                            <div class="col-md-2" style="text-align: center;">
                                <img src="<?php echo base_url(); ?>/assets/uploads/pkl/product/14716253_716744575142625_9025633034113160801_n.jpg" class="img-circle" alt="Profile Picture">
                            </div>
                            <div class="col-md-7" style="padding-left: 0;">
                                <p>
                                    <?php echo $v['first_name'].' '.$v['last_name']; ?>
                                    <br>
                                    <small><?php echo convert_time_ago($v['created_at']); ?></small>
                                </p>
                            </div>
                            <div class="col-md-3 news-feed-reaction">
                                <a href="#" data-toggle="tooltip" data-placement="right" title=""
                                   data-original-title="Komentar">
                                    <i class="zmdi zmdi-comment-outline zmdi-hc-lg"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title=""
                                   data-original-title="Aww!">
                                    <i class="zmdi zmdi-mood zmdi-hc-lg"></i>
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <br>
                <div class="news-feed-body">
                    <p>
                        <?php echo $v['description']; ?>
                    </p>
                    <?php
                        if(array_key_exists('image', $v)) {
                            ?>
                            <img src="<?php echo base_url().$v['image']['full_path']; ?>" class="img-responsive">
                            <?php
                        }
                    ?>
                </div>
            </div>
            <?php
        }
    }
?>