<div id="pkl-ads-list" class="owl-carousel">
    <?php
        for($i=0; $i<=4; $i++) {
    ?>
    <div class="tagkl-box" id="pkl-ads-box">
        <img
            src="<?php echo base_url(); ?>/assets/uploads/pkl/product/14716253_716744575142625_9025633034113160801_n.jpg"
            alt="">
        <h4>Cuangki dan Baso Ferdi <i class="zmdi zmdi-check-circle" title="Terverifikasi"></i></h4>
        <h5 class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci autem corporis esse
            explicabo facere, nam nemo quam vitae voluptas. Cumque harum, mollitia obcaecati odio perferendis porro
            quasi! Dolorum, quis, similique.</h5>
        <p class="text-muted"><i class="zmdi zmdi-pin"></i> in Padalarang</p>
        <br>
        <p>
            <div class="row">
                <div class="col-md-4">
                    <!--                    <p class="text-success">Sekarang Buka</p>-->
                <p class="text-danger">Sekarang Tutup</p>
                </div>
                <div class="col-md-4">
                    <a href="#">Kunjungi PKL</a>
                </div>
                <div class="col-md-4">
                    <p class="text-muted">Telah dibeli oleh 132 orang</p>
                </div>
            </div>
        </p>
        <a href="#">
            <div class="ads-info">
                <p class="no-margin"><i class="zmdi zmdi-info-outline"></i> Bersponsor</p>
            </div>
        </a>
    </div>
    <?php
    }
    ?>
</div>
