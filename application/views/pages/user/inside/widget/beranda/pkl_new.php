<h3>Baru Baru Ini</h3>
<div id="new-pkl-list" class="owl-carousel">
    <?php
        if($new_pkl) {
            foreach ($new_pkl as $v) {
                ?>
                <a href="#">
                    <div class="tagkl-box" id="new-pkl-box">
                        <?php
                            if($v['profile_picture']) {
                                ?>
                                <img src="<?php echo base_url() . '/' . $v['full_path']; ?>">
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo base_url() . '/assets/img/default-user.png'; ?>">
                                <?php
                            }
                        ?>
                        <h4><?php echo $v['product']; ?></h4>
                        <h5 class="text-muted">Oleh <?php echo $v['first_name'].' '.$v['last_name']; ?></h5>
                        <hr>
                        <p class="mdc-text-grey-500">Bergabung <?php echo convert_time_ago(date('Y-m-d H:i:s', $v['created_on'])); ?></p>
                    </div>
                </a>
                <?php
            }
        }
    ?>
</div>