<?php
    for($i=0; $i<=10; $i++) {
        ?>
        <div class="tagkl-box news-feed-box">
            <div class="news-feed-header">
                <a href="#">
                    <div class="row">
                        <div class="col-md-2" style="text-align: center;">
                            <img src="<?php echo base_url(); ?>/assets/uploads/pkl/product/14716253_716744575142625_9025633034113160801_n.jpg" class="img-circle img-responsive" alt="Profile Picture">
                        </div>
                        <div class="col-md-7" style="padding-left: 0;">
                            <p>
                                Ferdi Ferdiansyah
                                <br>
                                <small>1 Jam yang lalu</small>
                            </p>
                        </div>
                        <div class="col-md-3 news-feed-reaction">
                            <a href="#" data-toggle="tooltip" data-placement="right" title=""
                               data-original-title="Komentar">
                                <i class="zmdi zmdi-comment-outline zmdi-hc-lg"></i>
                            </a>
                            <a href="#" data-toggle="tooltip" data-placement="right" title=""
                               data-original-title="Aww!">
                                <i class="zmdi zmdi-mood zmdi-hc-lg"></i>
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <br>
            <div class="news-feed-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium eveniet
                    necessitatibus obcaecati, odio quod? Accusamus, autem, ducimus exercitationem illum maiores,
                    nisi nobis odio quae quisquam similique sunt voluptas voluptatibus?
                </p>
            </div>
        </div>
        <?php
    }
?>