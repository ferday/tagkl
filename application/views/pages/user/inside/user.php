<input type="hidden" id="user_id" value="<?php echo $user_profile['id']; ?>">
<div class="tagkl-profile-box">
    <div class="cover overlay grey" style="background-image: url('<?php echo base_url(); ?>assets/uploads/users/cover_picture/thumb-1920-473154.jpg');">
        <div class="col-md-7 profile-info">
            <div class="row">
                <div class="col-md-3">
                    <img src="<?php echo base_url().$user_profile['profile_picture']; ?>" class="img-circle">
                </div>
                <div class="col-md-8">
                    <p>
                        <?php
                        echo $user_profile['first_name']. ' ' .$user_profile['last_name'];
                        ?>
                    </p>
                    <?php
                        if($is_user_profile) {
                    ?>
                    <a href="<?php echo base_url('user/'.$user_profile['username'].'/settings'); ?>" class="btn btn-flat-default">Edit Profil</a>
                    <?php
                        } else {
                    ?>
                    <button class="btn btn-flat-default"><i class="zmdi zmdi-account-add"></i> Tambah sebagai teman</button>
                    <button class="btn btn-flat-default"><i class="zmdi zmdi-email"></i> Pesan</button>
                    <?php
                        }
                    ?>
                    <button class="btn btn-flat-default">Lainnya <i class="zmdi zmdi-chevron-down"></i></button>
                </div>
            </div>
        </div>
        <div class="col-md-5">

        </div>
    </div>
</div>
<br>
<?php
if(!$user_profile['status'] && $is_user_profile) {
    ?>
    <div class="alert alert-danger">
        <strong>Hai <?php echo $user_profile['first_name']; ?></strong> Sepertinya akunmu belum aktif sepenuhnya. Silahkan cek email untuk mengaktifkan akunmu sepenuhnya. Tidak menerima email ?
        <a href="#" class="alert-link">Kirim Ulang</a>
    </div>
    <?php
}
?>
<div class="row">
    <div class="col-md-3">
        <div class="tagkl-box in-profile">
            <h4 class="no-margin">
                Tentang
                <span class="pull-right">
                    <i class="zmdi zmdi-globe zmdi-hc-lg"></i>
                </span>
            </h4>
            <hr>
            <p class="intro-memo">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
            <ul>
                <li>Tinggal di Gunung Bentang</li>
                <li>Telah membeli jajanan dari PKL sebanyak <strong>23 kali</strong></li>
            </ul>
        </div>
        <div class="tagkl-box in-profile liked-pkl">
            <h4 class="no-margin">
                Yang <?php echo $user_profile['first_name']; ?> suka
                <span class="pull-right">
                    <i class="zmdi zmdi-mood zmdi-hc-lg"></i>
                </span>
            </h4>
            <hr>
        </div>
    </div>
    <div class="col-md-6">
        <?php
            if($is_user_profile) {
        ?>
        <div class="tagkl-box in-profile">
            <h4 class="no-margin">
                Perbarui Status
                <span class="pull-right">
                    <i class="zmdi zmdi-quote zmdi-hc-lg"></i>
                </span>
            </h4>
            <hr>
            <form id="update-status-form" enctype="multipart/form-data">
                <div class="form-group" id="tagkl-switch-status" style="padding:0; margin-bottom: 0;">
                    <textarea name="description" class="form-control" placeholder="Apa yang sedang kamu pikirkan ?" style="height: 70px;"></textarea>
                    <div class="image-container" style="display: none;">
                        <img class="img-preview">
                        <div class="after">
                            <p>
                                <i class="zmdi zmdi-close-circle-o zmdi-hc-3x"></i>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="" data-original-title="Sedang bersama seseorang"><i class="zmdi zmdi-accounts-add zmdi-hc-lg"></i></a>
                            <a href="#" class="fileUpload btn btn-default" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tambahkan foto">
                                <i class="zmdi zmdi-image zmdi-hc-lg"></i>
                                <input name="photo" type="file" class="upload" />
                            </a>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-danger"><i class="zmdi zmdi-mail-send"></i> Perbaharui</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
            }
        ?>
        <div id="status-list-wrap">
        </div>
    </div>
    <div class="col-md-3">
        <div class="tagkl-box">
            tes
        </div>
    </div>
</div>