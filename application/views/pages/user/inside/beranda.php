<?php
    if(!$user_info['status']) {
        ?>
        <div class="alert alert-danger">
            <strong>Hai <?php echo $user_info['first_name']; ?></strong> Sepertinya akunmu belum aktif sepenuhnya. Silahkan cek email untuk mengaktifkan akunmu sepenuhnya. Tidak menerima email ?
            <a href="#" class="alert-link">Kirim Ulang</a>
        </div>
        <?php
    }
?>
<div class="col-md-8">
    <div class="tagkl-welcome-message animated bounceInDown">
        <h1>
            Selamat <?php echo $current_date; ?>, <b><?php echo $user_info['first_name']; ?>!</b>
        </h1>
        <hr>
        <p>Apa yang mau kamu lakukan di <?php echo $current_date; ?> ini ?</p>
        <a href="#" class="btn btn-info btn-raised">Cari PKL</a>
        &nbsp;
        <a href="#" class="btn btn-info">Kepoin Profil Orang</a>
        &nbsp;
        <a href="#" class="btn btn-danger"><i class="zmdi zmdi-help-outline"></i> Bantuan TagKL</a>
    </div>
    <hr>
    <div class="tagkl-beranda-wrapper animated fadeIn">
        <div class="col-md-12 no-padding tagkl-beranda-section">
            <div class="tagkl-box" id="maps-box"></div>
            <div class="maps-info">
                <p class="no-margin"><i class="zmdi zmdi-my-location"></i> PKL Disekitarmu <small><a href="#">Lebih Lengkap</a></small></p>
            </div>
        </div>

        <div class="col-md-12 no-padding tagkl-beranda-section" id="pkl-new"></div>
        <div class="col-md-12 no-padding tagkl-beranda-section" id="pkl-ads"></div>
        <div class="col-md-12 no-padding tagkl-beranda-section">
            <div class="row">
                <div class="col-md-8">
                    <h3>Aktivitas Teman</h3>
                    <div id="news-feed"></div>
                </div>
                <div class="col-md-4">
                    <h3 class="text-right">PKL</h3>
                    <div id="news-feed-side-ads"></div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-md-4">
    <div id="right-side">
        <div class="row">
            <div class="col-md-12 no-padding tagkl-beranda-section">
                <br>
                <div class="tagkl-box" id="tagkl-switch-status" style="min-width: 100px">
                    <h4>
                        Perbarui Status
                        <span class="pull-right">
                            <i class="zmdi zmdi-quote zmdi-hc-lg"></i>
                        </span>
                    </h4>
                    <hr style="margin-top: 0;">
                    <form>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Apa yang sedang kamu pikirkan ?" style="height: 70px;"></textarea>
                            <div class="image-container" style="display: none;">
                                <img class="img-preview">
                                <div class="after">
                                    <p>
                                        <i class="zmdi zmdi-close-circle-o zmdi-hc-3x"></i>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="" data-original-title="Sedang bersama seseorang"><i class="zmdi zmdi-accounts-add zmdi-hc-lg"></i></a>
                                    <a href="#" class="fileUpload btn btn-default" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tambahkan foto">
                                        <i class="zmdi zmdi-image zmdi-hc-lg"></i>
                                        <input type="file" class="upload" />
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <button class="btn btn-danger btn-raised"><i class="zmdi zmdi-mail-send"></i> Perbaharui</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row" id="right-side-box">
            <div class="col-md-12 no-padding tagkl-beranda-section">
                <div class="tagkl-box"></div>
                <br>
                <div class="list-text">
                    <a href="#">Privasi</a>
                    <a href="#">Ketentuan</a>
                    <a href="#">Iklan</a>
                    <a href="#">Laporkan Masalah</a>
                </div>
                <p class="text-muted">&copy; TagMe 2016</p>
            </div>
        </div>
    </div>
</div>