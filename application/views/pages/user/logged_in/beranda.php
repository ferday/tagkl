<?php
	if($this->session->flashdata('alert')) {
?>
<div class="col-md-12" style="padding:0;">
	<div class="alert alert-danger" style="margin: 0;">
		<?php echo $this->session->flashdata('alert'); ?>
	</div>
</div>
<?php
	}
?>
<div class="col-md-12" id="maps-beranda" style="height: 500px;">
</div>
<div class="col-md-12">
	<div class="row">
	    <div class="col-md-9">
	    	<div id="recommend_pkl-beranda"></div>
	    	<div id="liked_pkl-beranda"></div>
	    	<div id="around_pkl-beranda"></div>
	    </div>
	    <div class="col-md-3 beranda-sidebar">
	        <h5 class="pull-left">
	        	BERSPONSOR
	        </h5>
	        <h5 class="pull-right">
	        	<a href="#">
		        	Buat Iklan
		        </a>
	        </h5>
	        <br>
	        <hr>
	        <?php
	        	for ($i=0; $i < 3; $i++) { 
	        ?>
	        <a href="#">
		        <div class="ads-list">
		        	<img src="<?php echo base_url('assets/img/background/pkl.jpg') ?>" class="img-responsive">
		        	<h4>Mie Baso Mang Jogre</h4>
		        	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		        	tempor incididunt ut labore et dolore magna aliqua.</p>
		        </div>
		    </a>
		    <?php
		    	}
		    ?>
	    </div>
    </div>
</div>