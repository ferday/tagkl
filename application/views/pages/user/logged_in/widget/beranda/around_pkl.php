<div class="pkl_list">
    <div class="row">
        <div class="col-md-10">
            <h3>Disekitar</h3>
            <p>Pedagang yang ada disekitar kamu.</p>
        </div>
        <div class="col-md-2">
            <a href="#" class="btn btn-success btn-raised btn-block">Lainnya &nbsp; <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <div class="row">
        <?php
            for ($i=0; $i <= 3; $i++) { 
            ?>
        <div class="col-md-3">
            <div class="kotak">
                <div class="kotak-sampul">
                    <img src="<?php echo base_url(); ?>assets/img/background/pkl.jpg" class="img-responsive">
                    <center>
                        <img src="<?php echo base_url(); ?>assets/img/user_profile/P_20160707_150851_BF.jpg" class="kotak-profile">
                    </center>
                </div>
                <div class="kotak-body">
                    <h4 class="text-center">Batagor Ferday</h4>
                    <div class="row">
                        <div class="col-md-8">
                            <p class="text-left" style="text-align:justify;">
                                Gunung Bentang, Padalarang
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p class="text-right">
                                <?php
                                    if($i == 2) {
                                    ?>
                                <span class="label label-success">Buka</span>
                                <?php
                                    } else {
                                    ?>
                                <span class="label label-danger">Tutup</span>
                                <?php
                                    }
                                    ?>
                            </p>
                        </div>
                    </div>
                    <button class="btn btn-success btn-raised btn-block">Lihat</button>
                    <p class="text-right" style="margin:0;">
                        <a href="#" style="color: #333;">
                        <i class="fa fa-flag-o"></i> Laporkan
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>