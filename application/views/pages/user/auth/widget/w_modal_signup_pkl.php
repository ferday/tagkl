<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title tagkl" style="color: #888;">TAGKL</h3>
			<p class="modal-sub-title" style="color: #95A5A6;">Untuk pedagang &nbsp;</p>
		</div>
		<div class="alert alert-danger">
			<strong>Daftar Sekarang Juga</strong> untuk mulai menjajakan dagangan ke <strong>213</strong> orang disekitarmu.
		</div>
		<div class="modal-body">
			<form id="pkl-register">
        		<div class="row">
          			<div class="col-md-6">
            			<div class="form-group ">
              				<label class="control-label" for="firstname">Nama Depan</label>
              				<input name="first_name" class="form-control" id="firstname" type="text" autocomplete="off" required>
            			</div>
          			</div>
			        <div class="col-md-6">
			        	<div class="form-group ">
			            	<label class="control-label" for="lastname">Nama Belakang</label>
			            	<input name="last_name" class="form-control" id="lastname" type="text" autocomplete="off" required>
			            </div>
			       	</div>
			    </div>
            	<div class="form-group">
              		<label class="control-label" for="emails">Dagangan</label>
              		<input name="product" class="form-control" autocomplete="off" required>
              		<p class="help-block">Contoh: Batagor MR.Ferday</p>
            	</div>
	            <div class="form-group">
              		<label class="control-label" for="emails">Email</label>
              		<input name="email" class="form-control" id="email" type="email" autocomplete="off" required>
              		<p class="text-danger" id="m_email"></p>
            	</div>
        		<div class="row">
          			<div class="col-md-8">
            			<div class="form-group ">
              				<label class="control-label" for="address">Alamat Lengkap</label>
              				<input name="address" class="form-control" type="text" autocomplete="off" required>
              				<p class="help-block">Contoh: Padalarang, Bandung Barat</p>
            			</div>
          			</div>
          			<div class="col-md-4">
            			<div class="form-group ">
              				<label class="control-label" for="postal_code">Kode Pos</label>
              				<input name="postal_code" class="form-control" type="text" autocomplete="off" required>
              				<p class="help-block">Contoh: 40553</p>
            			</div>
          			</div>
        		</div>
				<div class="form-group">
					<label class="control-label">
						Foto Dagangan
					</label>
					<br>
					<div class="fileUpload btn btn-danger">
						<span><i class="zmdi zmdi-image zmdi-hc-lg"></i> &nbsp; Unggah foto...</span>
						<input name="pkl_product" type="file" class="upload" />
					</div>
					<p class="fileUploadName" style="display: inline;"></p>
				</div>
        		<button type="submit" class="btn btn-danger btn-raised btn-block register-btn">Daftar Gratis</button>
        		<p>Dengan mendaftar, anda setuju dengan <a href="#">Syarat</a> dan <a href="#">Ketentuan</a> kami.</p>
      		</form>
		</div>
	</div>
</div>