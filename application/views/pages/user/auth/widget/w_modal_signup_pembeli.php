<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title tagkl" style="color: #888;">TAGKL</h3>
			<p class="modal-sub-title" style="color: #95A5A6;">Untuk partner perutmu</p>
		</div>
		<div class="alert alert-success">
			<strong>Daftar Sekarang Juga</strong> untuk mencari <strong>213</strong> pedagang yang sedang berjualan.
		</div>
		<div class="modal-body">
			<form id="register">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label" for="firstname">Nama Depan</label>
							<input name="f_name" class="form-control" type="text" autocomplete="off" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label" for="lastname">Nama Belakang</label>
							<input name="l_name" class="form-control" type="text" autocomplete="off" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="email">Email</label>
					<input name="email" class="form-control" type="email" autocomplete="off" required>
					<p class="help-block">Kode aktivasi akun akan dikirim kesini</p>
				</div>
				<div class="form-group">
					<label class="control-label" for="username">Nama Pengguna</label>
					<input name="username" class="form-control" type="text" autocomplete="off" required>
					<p class="text-danger" id="m_username"></p>
				</div>
				<div class="form-group">
					<label class="control-label" for="password">Kata Sandi</label>
					<input name="password" class="form-control" type="password" autocomplete="off" required>
				</div>
				<button type="submit" class="btn btn-success btn-raised btn-block register-btn">Daftar Gratis</button>
				<p>
					Atau daftar menggunakan &nbsp;
					<button class="btn btn-info btn-raised btn-sm"><i class="fa fa-facebook"></i> &nbsp; Facebook</button>
					<button class="btn btn-danger btn-raised btn-sm"><i class="fa fa-google-plus"></i> &nbsp; Google</button>
				</p>
			</form>
		</div>
	</div>
</div>