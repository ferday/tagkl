	<div class="col-md-6 col-md-push-3 signup-box">
		<div class="row">
			<div class="col-md-8">
				<h1>TagKL</h1>
				<p style="color: #777;">Bergabunglah dengan <strong><?php echo $total_user; ?></strong> pengguna lainnya</p>
			</div>
			<div class="col-md-4" style="padding-top:20px;">
				<a href="<?php echo base_url(); ?>" class="btn btn-raised btn-info btn-block btn-lg">Masuk</a>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<form method="POST" id="register">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label" for="firstname">Nama Depan</label>
								<input name="f_name" class="form-control" type="text" autocomplete="off" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label" for="lastname">Nama Belakang</label>
								<input name="l_name" class="form-control" type="text" autocomplete="off" required>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label" for="emails">Email</label>
								<input name="email" class="form-control" type="email" autocomplete="off" required>
								<p class="help-block">Kode aktivasi akun akan dikirim kesini</p>
							<div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label" for="username">Nama Pengguna</label>
								<input name="username" class="form-control" type="text" autocomplete="off" required>
								<p class="text-danger" id="m_username"></p>
							<div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label" for="password">Kata Sandi</label>
								<input name="password" class="form-control" type="password" autocomplete="off" required>
							<div>
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								&nbsp; Saya telah setuju dengan <a href="#">Syarat</a> dan <a href="#">Ketentuan</a> TagKL
							</label>
						</div>
					</div>
					<button type="submit" id="submit" class="btn btn-raised btn-info">Daftar</button> &nbsp; <font style="color:#777;">atau daftar menggunakan</font> <a href="#" class="btn btn-info"><i class="fa fa-facebook"></i></a> <a href="#" class="btn btn-danger"><i class="fa fa-google-plus"></i></a>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal animated bounceInDown" id="success">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header-new">
					<h1 class="text-center" style="color:#FFF;"><i class="fa fa-hand-spock-o"></i></h1>
					<h3 class="modal-title text-center">Pendaftaran Berhasil</h3>
				</div>
				<div class="modal-body" style="color:#777;">
					<div id="modal-text">
					</div>
					<hr>
					<br>
					<h3 class="text-center">TagKL</h3>
					<h4 class="text-center">Your Belly's Partner!</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Oke Saya Mengerti</button>
				</div>
			</div>
		</div>
	</div>
