<style>
	html,body,.col-md-12,.row {
		height:100%;
	}
	.row > div {
		height:100%;
	}
	.row > .col-md-4 {
		background: #27376B;
		padding: 0 30px 0 30px;
	}
	.auth-logo {
		background: #DA4624;
		padding: 25px 15px 25px 15px;
		width: 120px;
		text-align: center;
		transition: 0.5s;
		cursor: pointer; ybn6
	}
	.auth-logo h2{
		margin: 0;
		color: #FAFAFA;
		font-weight: 600;
	}
	.auth-logo:hover {
		width: 140px;
	}
	.auth-content {
		margin-top: 100px;
	}
	.quote {
		line-height: 1.5;
	}
	.quote .quote-from {
		color: #FFF;
		font-weight: 500;
		font-size: 18px;
	}
	.quote-from small {
		color: #F8F8F8;
	}
	.quote .hr {
		width: 150px;
		border: 2px solid #FFF;
		border-radius: 5px;
		margin-top: 5px;
	}
	.quote .quote-title {
		color: #FFF;
	}
	.quote .quote-body {
		margin-right: 50px;
		color: #FFF;
		text-align: justify;
		font-weight: 400;
	}
	.auth-content .auth-signup {
		margin-top: 50px;
	}
	.auth-signup p{
		color: #FFF;
		font-weight: 500;
	}
	.btn.btn-flat-danger {
		background: #DA4624;
		color: #FFF;
	}
	.btn.btn-flat-success {
		background: #4EC487;
		color: #FFF;
	}
	.auth-footer {
		position: absolute;
		right: 15px;
		bottom: 15px;
		color: #E5E5E5;
	}
</style>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-4">
        	<div class="auth-logo">
        		<h2>TagKL</h2>
        	</div>
        	<div class="auth-content">
        		<div class="quote">
	        		<p class="quote-from">
	        			Kesan dari <i>Ferdi Ferdiansyah</i> - <small>Pedagang</small>
	        		</p>
	        		<div class="hr"></div>
	        		<h2 class="quote-title">
	        			Sangat membantu
	        		</h2>
	        		<p class="quote-body">
	        			Semenjak saya berjualan sambil menggunakan aplikasi TagKL ini dagangan saya makin laku.
	        		</p>
        		</div>
        		<div class="hr"></div>
        		<div class="auth-signup">
        			<p class="text-center">Daftar Sekarang Juga</p>
        			<div class="row">
        				<div class="col-md-6">
        					<button class="btn btn-flat-danger btn-block" >Saya Pedagang</button>
        				</div>
        				<div class="col-md-6">
        					<button class="btn btn-flat-danger btn-block">Saya Pembeli</button>
        				</div>
        			</div>
        		</div>
        		<div class="auth-footer">
        			<p>&copy; TagMe 2016</p>
        		</div>
        	</div>
        </div>
        <div class="col-md-8 auth-body">
            Body content
        </div>
    </div>
</div>