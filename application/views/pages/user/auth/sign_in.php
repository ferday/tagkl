<div class="container-fluid">
    <div class="row">
        <div class="col-md-7 left-side">
            <div class="content-top animated fadeIn">
            	<a href="<?php echo base_url(); ?>" class="tagkl-brand">
	                <h1>TagKL</h1>
	                <h5>Your Belly's Partner</h5>
	            </a>
            </div>
            <div class="content-middle animated fadeIn">
                <h2>Selamat datang di <strong class="tagkl">TagKL</strong></h2>
                <p>Cari pedagang makanan disekitar ketika kamu lapar akan lebih mudah dengan menggunakan <strong class="tagkl">TagKL</strong></p>
                <br>
                <a class="btn btn-raised btn-default signup-btn" data-type="pembeli">Daftar</a>
                &nbsp; atau
                <a class="btn btn-default signup-btn" data-type="pkl" style="color: #FFF;">Daftar sebagai pedagang</a>
            </div>
        </div>
        <div class="col-md-5" style="padding:100px 50px 0 50px;">
            <div class="login-box">
            	<header>
            		<h4 class="text-center">Masuk ke <font class="tagkl">TagKL</font></h4>
            		<p class="text-center">Untuk mencari partner perutmu</p>
            	</header>
            	<div class="login-body">
	                <form class="form-horizontal" id="signin-form">
					    <div class="form-group col-md-12">
					        <label class="control-label" for="inputDefault">Nama Pengguna</label>
					        <input type="text" class="form-control" name="username" autocomplete="off">
					    </div>
					    <div class="form-group col-md-12">
					        <label class="control-label" for="inputDefault">Kata Sandi</label>
					        <input type="password" class="form-control" name="password">
				        </div>
				        <div class="form-group col-md-12">
				        	<div class="checkbox">
								<label>
							    	<input type="checkbox" value="" name="remember">
							        &nbsp; Ingat Saya
							    <label>
							</div>
				        </div>
				        <div class="form-group col-md-12">
						    <button type="submit" class="btn btn-raised btn-success btn-block" name="submit">Masuk</button>
						</div>
					</form>
					<p>Kesulitan mengakses akun? <a href="#" onclick="tes()">Klik disini</a></p>
				</div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="signup-modal">
	
</div>