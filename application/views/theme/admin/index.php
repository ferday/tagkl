<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<title><?php echo $title; ?></title>

		<link rel="shortcut icon" href="<?php echo base_url(config_item('app_favico')); ?>">
		<?php $this->load->view('theme/admin/css'); ?>
		<?php $this->load->view('theme/admin/js'); ?>

	</head>
	<body class="hold-transition skin-red sidebar-collapse sidebar-mini">
		<div class="wrapper">

			<!-- Header -->
			<?php $this->load->view('theme/admin/header'); ?>

			<!-- Menu -->
			<?php $this->load->view('theme/admin/menu'); ?>

			<div class="content-wrapper">
				<section class="content-header">
					<h1>
				    	<?php echo $title; ?>
				    </h1>
				    <br>
				</section>
				<section class="content">
					<!-- Content -->
					<?php
						// CSS
						$this->load->view('theme/admin/_css');
						// Page
						if($content) {
							$this->load->view($content);
						}
						// JS
						$this->load->view('theme/admin/_js');
					?>
				</section>
			</div>

			<!-- Footer -->
			<?php $this->load->view('theme/admin/footer'); ?>
		</div>
	</body>
</html>