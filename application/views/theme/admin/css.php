<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/font-awesome-4.6.3/css/font-awesome.css">

<!-- Theme style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/AdminLTE/css/AdminLTE.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/AdminLTE/css/skin-red.min.css">

<!-- Ion Icon -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/ionicon/css/ionicons.css">

<!--NProgress-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/nprogress/css/nprogress.css">

<!--Toastr-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/toastr/css/toastr.css">

<!--Global-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/global.css">