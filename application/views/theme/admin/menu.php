<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url(); ?>assets/custom/img/assets/1.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;?></p>
                Admin
            </div>
        </div>
        <ul class="sidebar-menu">
            <li>
                <a href="<?php echo base_url(); ?>admin/">
                    <i class="ion ion-ios-pulse-strong"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>PKL Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url(); ?>admin/add_pkl/">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/manage_pkl/">
                            <i class="fa fa-bars"></i> Manage
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
</aside>