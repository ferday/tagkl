<div class="row">
    <div class="col-md-12">
        <i class="zmdi zmdi-menu zmdi-hc-2x mdc-text-black-dark toggle-menu"></i>
    </div>
</div>
<?php
    if($with_title) {
?>
<div class="row">
    <div class="col-md-12">
        <h3 class="mdc-text-grey-500"><?php echo $title; ?></h3>
        <hr>
    </div>
</div>
<?php
    } else {
        echo '<br>';
    }
?>
<?php
//    if($account_status) {
//        if(!$account_status['status']) {
//?>
<!--<div class="row">-->
<!--    <div class="col-md-12">-->
<!--        <div class="alert alert---><?php //echo $account_status['type']; ?><!--">-->
<!--        	--><?php //echo $account_status['message']; ?>
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<?php
//        }
//    }
//?>