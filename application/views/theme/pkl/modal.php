<div class="modal fade in modal-middle" id="modal-set-default-location">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center">
                    <i class="zmdi zmdi-shopping-basket zmdi-hc-3x animated swing mdc-text-red-500"></i>
                    <br>
                    <span class="mdc-text-grey-600">Mulai Menggunakan TagKL</span>
                </h3>
            </div>
            <div class="modal-body">
                <h4 class="text-center">
                    Dimana biasanya anda memulai berjualan ?
                </h4>
                <hr>
                <div id="set-location-container">
                    <div class="before">
                        <h5 class="text-center">Pilih Metode Menentukan Lokasi</h5>
                        <center>
                            <a href="#" class="btn btn-info map-btn"><i class="zmdi zmdi-google-maps"></i> Buka Peta</a>
                            &nbsp;
                            <a href="#" class="btn btn-success manual-btn"><i class="zmdi zmdi-edit"></i> Masukkan Manual</a>
                        </center>
                    </div>
                    <div class="choose-location-by-map-container hidden">
                        <p class="text-right"><i class="zmdi zmdi-pin animated jello infinite mdc-text-red-500"></i> <span class="mdc-text-grey-500">Geser pin tepat pada lokasimu sekarang</span></p>
                        <div id="set-location-container-map" style="height: 300px;"></div><br>
                        <div class="row">
                            <div class="col-md-8">
                                <p class="mdc-text-grey-400">Pastikan anda mengaktifkan pengaturan lokasi pada perangkat anda. <a href="#">Bantuan</a></p>
                            </div>
                            <div class="col-md-4">
                                <form id="save-location-form">
                                    <input type="hidden" name="lat">
                                    <input type="hidden" name="lng">
                                    <button class="btn btn-info btn-block" type="submit"><i class="zmdi zmdi-check"></i> Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <form id="set-location-manual-form" class="hidden">
                        <div class="form-group">
                            <label class="mdc-text-grey-500" style="font-weight: inherit">Masukkan Alamat</label>
                            <input class="form-control" style="border-radius: 0;">
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <p class="mdc-text-grey-500">Ingin lebih mudah? <a href="#" class="map-btn">Buka Map</a></p>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-info btn-block"><i class="zmdi zmdi-check"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                <p class="text-center mdc-text-grey-500">
                    <i class="zmdi zmdi-info-outline"></i> Lokasi membantu orang menemukan anda
                </p>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>