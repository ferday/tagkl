<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <h2 class="tagkl text-center" style="color: #F44336">TagKL</h2>
        <br>
        <img src="<?php echo base_url('assets/img/default-user.png'); ?>" class="profile-picture img-circle">
        <br>
        <h4 class="text-center mdc-text-grey-700" style="margin: 0 5px 5px 5px; word-wrap: break-word"><?php echo $pkl['first_name'].' '.$pkl['last_name']; ?></h4>
        <br>
        <li <?php echo strtolower(uri_string()) == 'pkl' ? 'class="active"' : ''; ?>>
            <a href="<?php echo base_url('pkl'); ?>"><i class="zmdi zmdi-home zmdi-hc-lg zmdi-hc-fw"></i> Beranda</a>
        </li>
        <li <?php echo strtolower(uri_string()) == 'pkl/gerobak' ? 'class="active"' : ''; ?>>
            <a href="#"><i class="zmdi zmdi-shopping-cart zmdi-hc-lg zmdi-hc-fw"></i> Gerobak</a>
        </li>
        <li <?php echo strtolower(uri_string()) == 'pkl/profile' ? 'class="active"' : ''; ?>>
            <a href="<?php echo base_url('pkl/profile'); ?>"><i class="zmdi zmdi-account-box zmdi-hc-lg zmdi-hc-fw"></i> Profil</a>
        </li>
        <li>
            <a href="<?php echo base_url('auth/logout'); ?>"><i class="zmdi zmdi-square-right zmdi-hc-lg zmdi-hc-fw"></i> Logout</a>
        </li>
    </ul>
</div>