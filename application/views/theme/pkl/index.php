<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
<!--		<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<title><?php echo $title; ?></title>

		<link rel="shortcut icon" href="<?php echo base_url(config_item('app_favico')); ?>">
		<?php $this->load->view('theme/pkl/css'); ?>
		<?php $this->load->view('theme/pkl/js'); ?>

	</head>
	<body>
		<div id="wrapper">
			<!-- Menu -->
			<?php $this->load->view('theme/pkl/menu'); ?>

			<div id="page-content-wrapper">
				<div class="container-fluid">
					<?php
						// Header
						$this->load->view('theme/pkl/header');
						// CSS
						$this->load->view('theme/pkl/_css');
						// Page
						if($content) {
							$this->load->view($content);
						}
						// Modal
						$this->load->view('theme/pkl/modal');
						// JS
						$this->load->view('theme/pkl/_js');
					?>
				</div>
			</div>

		</div>
		<!-- Footer -->
		<?php $this->load->view('theme/pkl/footer'); ?>
	</body>
</html>