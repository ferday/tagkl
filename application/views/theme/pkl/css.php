<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">

<!--NProgress-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/nprogress/css/nprogress.css">

<!--Toastr-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/toastr/css/toastr.css">

<!--Icon & Color-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/material-design-iconic-font/css/material-design-iconic-font.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/color-pallete/material-design-color-palette.min.css">

<!--Animate CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/animate.css">

<!--Global-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/global.css?v=<?php echo time(); ?>">

<!--PKL-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/pkl.css?v=<?php echo time(); ?>">