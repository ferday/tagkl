<script>
    var base_url = "<?php echo base_url(); ?>";
	var site_url = "<?php echo base_url(); ?>index.php/";
    var loading = '<div class="loader-pkl">'
        + '<span class="dot dot_1"></span>'
        + '<span class="dot dot_2"></span>'
        + '<span class="dot dot_3"></span>'
        + '<span class="dot dot_4"></span>'
        + '</div>';
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/jquery-3.1.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/nprogress/js/nprogress.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/toastr/js/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/gmaps/gmaps.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/jquery-geolocation/jquery.geolocation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/pkl/global.js?v=<?php echo time(); ?>"></script>
