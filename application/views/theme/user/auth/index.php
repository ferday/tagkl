<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<title><?php echo $title; ?></title>

		<link rel="shortcut icon" href="<?php echo base_url(config_item('app_favico')); ?>">
		<?php $this->load->view('theme/user/auth/css'); ?>

		<?php
			$rand = rand(0, (count($background)-1));
			$background = $background[$rand]['path'].$background[$rand]['name'];
		?>
		<style>
			.auth-body {
				background-image: url("<?php echo base_url().$background; ?>");
			    background-image: linear-gradient(to right, rgba(0,0,0, 0.7), rgba(0,0,0, 0.3)),url("<?php echo base_url().$background; ?>");
			}
		</style>
	</head>
	<body class="auth-body">
		<?php
			// CSS
			$this->load->view('theme/user/auth/_css');
			// Page
			if($content) {
				$this->load->view($content);
			}
			// Modal
			$this->load->view('theme/user/auth/modal');
			// JS
			$this->load->view('theme/user/auth/js');
			$this->load->view('theme/user/auth/_js');
		?>
	</body>
</html>