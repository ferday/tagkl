<script>
	var site_url = "<?php echo base_url(); ?>index.php/";
	var base_url = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/jquery-3.1.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/material/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/material/js/ripples.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/nprogress/js/nprogress.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/app.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/snackbar/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/toastr/js/toastr.min.js"></script>