<div class="col-md-12 t-navbar">
	<div class="row">
		<div class="container-fluid">
			<div class="col-md-2 col-sm-2">
				<h3 class="t-navbar-title">TagKL</h3>
			</div>
			<div class="col-md-8 col-sm-8">
				<center>
					<a href="#">
						<div class="t-navbar-menu active">
							<h5>
								<i class="fa fa-home"></i><br>Beranda
							</h5>
						</div>
					</a>
					<a href="#">
						<div class="t-navbar-menu">
							<h5>
								<i class="fa fa-newspaper-o"></i><br>Kabar Berita
							</h5>
						</div>
					</a>
					<a href="#">
						<div class="t-navbar-menu">
							<h5>
								<i class="fa fa-globe"></i><br>Jelajah
							</h5>
						</div>
					</a>
				</center>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="btn-group btn-block t-navbar-profile">
	                <a href="javascript:;" data-target="dropdown-menu" class="btn btn-danger btn-block dropdown-toggle" data-toggle="dropdown">
	                	<img src="<?php echo base_url(); ?>assets/img/user_profile/P_20160707_150851_BF.jpg">
		            	&nbsp; Ferdi
	                  	<span class="caret"></span>
	                </a>
	                <ul class="dropdown-menu btn-block">
	                	<li><a href="javascript:void(0)"><i class="fa fa-user"></i> &nbsp; &nbsp; &nbsp; Profil</a></li>
	                	<li><a href="javascript:void(0)"><i class="fa fa-wrench"></i> &nbsp; &nbsp; &nbsp; Pengaturan</a></li>
		            	<li><a href="<?php echo base_url('auth/logout'); ?>"><i class="fa fa-sign-out"></i> &nbsp; &nbsp; &nbsp; Keluar</a></li>
	                </ul>
              </div>
			</div>
		</div>
	</div>
</div>