<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<title><?php echo $title; ?></title>

		<link rel="shortcut icon" href="<?php echo base_url(config_item('app_favico')); ?>">
		<?php $this->load->view('theme/user/logged_in/css'); ?>
	</head>
	<body>
		<?php
			$this->load->view('theme/user/logged_in/header');
			// CSS
			$this->load->view('theme/user/logged_in/_css');
			// Page
			if($content) {
				$this->load->view($content);
			}
			// JS
			$this->load->view('theme/user/logged_in/js');
			$this->load->view('theme/user/logged_in/_js');
		?>
	</body>
</html>