<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">

<!-- Bootstrap Material Design -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/material/css/bootstrap-material-design.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/material/css/ripples.min.css">

<!-- External -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/font-awesome-4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/nprogress/css/nprogress.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/snackbar/css/snackbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/snackbar/css/material.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/toastr/css/toastr.css">

<!-- Custom -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/opensans.css">

<!--Global-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/global.css">