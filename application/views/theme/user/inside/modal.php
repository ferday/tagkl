<div class="modal modal-middle animated bounceIn" id="like-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mdc-text-grey-600">Orang yang menyukai ini</h4>
            </div>
            <div class="modal-body tagkl-like-detail">
            </div>
        </div>
    </div>
</div>
<div class="modal modal-middle animated bounceIn" id="remove-status">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mdc-text-grey-600">Hapus Kiriman</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>