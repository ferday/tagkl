<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php echo $title; ?></title>

		<link rel="shortcut icon" href="<?php echo base_url(config_item('app_favico')); ?>">
		<?php $this->load->view('theme/user/inside/css'); ?>
	</head>
	<body>
		<?php
			$this->load->view('theme/user/inside/header');
			// CSS
			$this->load->view('theme/user/inside/_css');
		?>
		<div class="tagkl-content">
			<div class="col-md-1">
				<?php
					// Menu
					$this->load->view('theme/user/inside/menu');
				?>
			</div>
			<div class="col-md-11">
				<?php
					// Page
					if($content) {
						$this->load->view($content);
					}
				?>
			</div>
		</div>
		<?php
			// Modal
			$this->load->view('theme/user/inside/modal');

			// JS
			$this->load->view('theme/user/inside/js');
			$this->load->view('theme/user/inside/_js');
		?>
	</body>
</html>