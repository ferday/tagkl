<div class="tagkl-menu-left-wrapper">
    <div class="tagkl-menu-left-list active" data-toggle="tooltip" data-placement="right" title="" data-original-title="Beranda">
        <i class="zmdi zmdi-collection-text zmdi-hc-3x"></i>
    </div>
    <div class="tagkl-menu-left-list" data-toggle="tooltip" data-placement="right" title="" data-original-title="Jelajahi TagKL">
        <i class="zmdi zmdi-globe zmdi-hc-3x"></i>
    </div>
    <div class="tagkl-menu-left-list" data-toggle="tooltip" data-placement="right" title="" data-original-title="Riwayat">
        <i class="zmdi zmdi-undo zmdi-hc-3x"></i>
    </div>
</div>