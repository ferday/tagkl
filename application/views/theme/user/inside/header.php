<div class="tagkl-navbar">
    <div class="col-md-2">
        <a href="<?php echo base_url(); ?>">
            <div class="tagkl-brand">
                <h3 class="text-left"><span class="tagkl">TagKL</span> <small class="text-danger">Beta</small></h3>
            </div>
        </a>
    </div>
    <div class="col-md-7">
        <div class="tagkl-search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Cari Pengguna, Pedagang Kaki Lima, Lokasi, dan lainnya...">
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-8">
                <div class="pull-right">
                    <div class="tagkl-notif-bar">
                        <a href="#">
                            <i class="zmdi zmdi-account-add"></i>
                        </a>
                        <a href="#">
                            <i class="zmdi zmdi-email"></i>
                        </a>
                        <a href="#">
                            <i class="zmdi zmdi-notifications"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pull-left">
                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                        <div class="tagkl-profile-bar">
                            <img src="<?php echo base_url().$user_info['profile_picture']; ?>">
                        </div>
                    </a>
                    <ul class="dropdown-menu pull-right profile-dropdown" style="width: 300px;">
                        <img src="<?php echo base_url().$user_info['profile_picture']; ?>">
                        <a href="<?php echo base_url().'user/'.$user_info['username']; ?>">
                            <h4><?php echo $user_info['first_name'].' '.$user_info['last_name']; ?></h4>
                            <p class="text-muted" style="font-size: 12px;"><i class="zmdi zmdi-my-location"></i> Padalarang, Jawabarat</p>
                        </a>
                        <div class="col-md-6">
                            <button class="btn btn-info">Pengaturan</button>
                        </div>
                        <div class="col-md-6">
                            <a href="<?php echo base_url(); ?>auth/logout" class="btn btn-danger">Keluar</a>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>