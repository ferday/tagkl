<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">

<!-- Bootstrap Material Design -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/material/css/bootstrap-material-design.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/material/css/ripples.min.css">

<!-- External -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/nprogress/css/nprogress.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/toastr/css/toastr.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/material-design-iconic-font/css/material-design-iconic-font.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lib/color-pallete/material-design-color-palette.min.css">
<!-- Custom -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/style.css">

<!--Global-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/custom/css/global.css">