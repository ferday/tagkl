<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Admin_m extends MY_Model {

        public function __construct() {
            parent::__construct();
        }

        public function check_user($table = null, $data = array()) {
            $check = $this->db->get_where($table, $data)->num_rows();
            if($check < 1) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

?>