<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Pkl_model extends MY_Model {
        public function __construct() {
            parent::__construct();
        }

        public function get_pkl($id = NULL, $params = array(), $additional_fields = array(), $is_row = FALSE) {
            if($additional_fields) {
                $this->db->select(implode(',', $additional_fields));
            } else {
                $this->db->select('pkl.id, pkl.product, pkl.status, pkl.lat, pkl.lng, users.id as `user_id`, users.first_name, users.last_name, users.email, users.address, users.postal_code, users.profile_picture, users.created_on, image.full_path');
            }
            if($id) {
                $this->db->where('pkl.id', $id);
            }
            if($params) {
                foreach($params as $k => $v) {
                    $this->db->where($k, $v);
                }
            }
            $this->db->join('users', 'users.id = pkl.user_id', 'left');
            $this->db->join('image', 'image.id = users.profile_picture', 'left');
            $this->db->order_by('users.created_on', 'DESC');
            $result = $this->db->get('pkl');

            if($id || $is_row) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        }

        public function get_pkl_detail($id, $with_user = false) {
            $this->db->select('pkl.id, pkl.first_name, pkl.last_name, pkl.product, pkl.email, pkl.address, pkl.postal_code, pkl.profile_picture, pkl.created_at, pkl.status');
            $this->db->where('pkl.id', $id);
            if($with_user) {
                $this->db->join();
            }
            return $this->db->get('pkl')->row_array();
        }

        public function get_new_pkl($limit = 3) {
            $this->db->select('pkl.id, pkl.first_name, pkl.last_name, pkl.product, pkl.email, pkl.address, pkl.postal_code, pkl.profile_picture, pkl.created_at, pkl.status, image.id as "image_id", image.full_path');
            $this->db->where('status', 1);
            $this->db->limit($limit);
            $this->db->order_by('created_at', 'desc');
            $this->db->join('image', 'image.id = pkl.profile_picture', 'left');
            return $this->db->get('pkl');
        }

        public function request_action($id, $status) {
            $pkl = $this->get_pkl($id);
            switch($status) {
                case 1:
                    $body = 'Selamat!';
                    break;
                case 2:
                    $body = 'Mohon maaf';
                    break;
                default:
                    $body = 'Mohon maaf';
                    break;
            }
            $data_mailing = array(
                'subject' => 'TagKL Account Information',
                'email_to' => $pkl['email'],
                'body' => $body
            );
            $this->mailing_log($data_mailing);

            if($this->db->update('pkl', array('status' => $status), array('id' => $id))) {
                $user_id = $this->get_pkl($id, null, array('pkl.user_id'));
                $this->ion_auth->update($user_id['user_id'], array('status' => $status));
                return $id;
            } else {
                return false;
            }
        }

        public function get_total($status) {
            $this->db->where('status', $status);
            return $this->db->get('pkl')->num_rows();
        }
    }
?>