<?php

    class Status_model extends MY_Model {
        function __construct() {
            parent::__construct();
        }

        function get_status($user_id, $offset = 0, $status_id = FALSE) {
            $this->load->helper('global_helper');
            $this->db->select(
                'status.id, status.description, status.created_at,
                users.id as `user_id`, users.first_name, users.last_name, users.profile_picture,
                status_image.image_id'
            );
            if($status_id) {
                $this->db->where('status.id', $status_id);
            }
            $this->db->where('status.user_id', $user_id);
            $this->db->join('users', 'users.id = status.user_id');
            $this->db->join('status_image', 'status_image.status_id = status.id', 'left');
            $this->db->order_by('status.created_at', 'DESC');
            $this->db->limit(10, $offset);
            $status_list = $this->db->get('status')->result_array();
            $status = array();
            if($status_list) {
                foreach($status_list as $k => $v) {
                    if($v['profile_picture']) {
                        $image = $this->get_image_status($v['profile_picture']);
                        $v['profile_picture'] = $image['full_path'];
                    } else {
                        $v['profile_picture'] = './assets/img/default-user.png';
                    }
                    $v['time_ago'] = convert_time_ago($v['created_at']);
                    if($v['image_id']) {
                        $image = $this->get_image_status($v['image_id']);
                        $v['image'] = $image;
                    }
                    $v['like'] = $this->get_like_status($v['id'], TRUE);
                    $v['liked_by_me'] = $this->get_like_status($v['id'], TRUE, $this->ion_auth->user()->row()->id) ? TRUE : FALSE;
                    $v['yours'] = $v['user_id'] == $this->ion_auth->user()->row()->id ? TRUE : FALSE;

                    unset($v['image_id'], $v['user_id']);
                    $status[] = $v;
                }
            }

            return $status;
        }

        function get_image_status($image_id) {
            $this->db->select('id, full_path');
            $this->db->where('id', $image_id);
            return $this->db->get('image')->row_array();
        }

        function get_like_status($status_id, $count = FALSE, $user_id = NULL) {
            $this->db->select('users.username, users.first_name, users.last_name, users.profile_picture');
            $this->db->where('status_id', $status_id);
            if($user_id) {
                $this->db->where('users.id', $user_id);
            }
            $this->db->join('users', 'users.id = status_like.user_id', 'left');
            $query = $this->db->get('status_like');
            if($count) {
                return $query->num_rows();
            } else {
                $status = $query->result_array();

                if($status) {
                    foreach($status as $k => $v) {
                        if($v['profile_picture']) {
                            $image = $this->get_image_status($v['profile_picture']);
                            $status[$k]['profile_picture'] = $image['full_path'];
                        } else {
                            $status[$k]['profile_picture'] = './assets/img/default-user.png';
                        }
                    }
                }

                return $status;
            }
        }

        function remove($id) {
            $this->db->select('user_id, image_id');
            $this->db->join('status_image', 'status_image.status_id = status.id', 'left');
            $detail = $this->db->get_where('status', array('id' => $id))->row_array();
            if($this->ion_auth->user()->row()->id == $detail['user_id']) {
                $image = $this->get_image_status($detail['image_id']);
                if($image) {
                    unlink($image['full_path']);
                    $this->db->delete('image', array('id' => $image['id']));
                }

                $this->db->delete('status', array('id' => $id));
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }