<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth_m extends MY_Model {

		public function __construct() {
			parent::__construct();
		}

		public function get_background() {
			return $this->db->get('background');
		}

		public function get_total_user() {
			return $this->db->get('users')->num_rows();
		}

		public function check_user($table = null, $data = array()) {
			$check = $this->db->get_where($table, $data)->num_rows();
			if($check < 1) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		public function mailing($id) {
		    $user_detail = $this->db->get_where('users', array('id' => $id))->row_array();
            $key = md5(base64_encode($user_detail['email']));
			$message = 'Dear '. $user_detail['first_name'] .',<br /><br />
		                Please click on the below activation link to verify your email address.<br /><br />
		                '.base_url().'action/verify/' . $key . '<br /><br /><br />
		                Thanks<br />
		                Nuroaki';

            $data = array(
                'subject' => 'TagKL Account Information',
                'email_to' => $user_detail['email'],
                'body' => $message
            );
		    $this->mailing_log($data);
		}

		public function insert($table = null, $data = array()) {
			$this->db->insert($table, $data);
			return $this->db->insert_id();
		}

	}

?>