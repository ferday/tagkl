<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends MY_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_user_profile($username) {
        $this->db->where('username', $username);
        $this->db->where('active', 1);
        $user = $this->db->get('users')->row_array();

        if($user) {
            if($user['profile_picture']) {
                $this->db->select('full_path');
                $this->db->where('id', $user['profile_picture']);
                $image = $this->db->get('image')->row_array();

                $user['profile_picture'] = $image['full_path'];
            } else {
                $user['profile_picture'] = './assets/img/default-user.png';
            }
        }

        return $user;
    }

    public function check_user($table = null, $data = array()) {
        $check = $this->db->get_where($table, $data)->num_rows();
        if($check < 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>