<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Async extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->model('status_model');
        }

        public function get_status() {
            $user_id = $this->input->post('user_id');
            $page = $this->input->post('page') ? $this->input->post('page') * 10 : null;

            $status = $this->status_model->get_status($user_id, $page);

            $result = array(
                'status' => true,
                'message' => 'success',
                'data' => $status
            );
            $this->json_result($result);
        }

        public function update_status() {
            $description = $this->input->post('description');
            $user_info = $this->user_info('true');
            $image = $this->do_upload('photo', './assets/uploads/users/photo/', 'jpg|jpeg|png|gif');

            $data = array(
                'description' => $description,
                'created_at' => date('Y-m-d H:i:s'),
                'user_id' => $user_info->id
            );

            $this->db->insert('status', $data);
            $status_id = $this->db->insert_id();

            $image_id = null;
            if($image) {
                $image_data = array(
                    'name' => $image['file_name'],
                    'type' => $image['file_type'],
                    'size' => $image['file_size'],
                    'path' => './assets/uploads/users/photo/',
                    'full_path' => './assets/uploads/users/photo/'.$image['file_name']
                );
                $this->db->insert('image', $image_data);
                $image_id = $this->db->insert_id();
            }

            if($image_id) {
                $status_image = array(
                    'status_id' => $status_id,
                    'image_id' => $image_id
                );
                $this->db->insert('status_image', $status_image);
            }

            $status = $this->status_model->get_status($user_info->id, null, $status_id);

            $result = array(
                'status' => true,
                'message' => 'Success',
                'data' => $status
            );
            $this->json_result($result);
        }

        public function status_action($type) {
            $status_id = $this->input->post('id');
            $user_id = $this->user_info(true);
            switch($type) {
                case 'like':
                    $data = array(
                        'status_id' => $status_id,
                        'user_id' => $user_id->id
                    );
                    $liked = $this->db->get_where('status_like', $data)->num_rows() ? TRUE : FALSE;
                    if($liked) {
                        $this->db->delete('status_like', $data);
                        $type = 'unlike';
                    } else {
                        $this->on_duplicate('status_like', $data);
                        $type = 'like';
                    }

                    $result = array(
                        'status' => true,
                        'message' => 'Success',
                        'type' => $type
                    );
                    break;

                case 'like_detail':
                    $like_list = $this->status_model->get_like_status($status_id);
                    $result = array(
                        'status' => true,
                        'message' => 'Success',
                        'like' => $like_list
                    );
                    break;

                case 'remove':
                    $remove = $this->status_model->remove($status_id);
                    $result = array(
                        'status' => $remove,
                        'message' => $remove ? 'Success' : 'Failed',
                        'id' => $status_id
                    );
                    break;
            }

            $this->json_result($result);
        }

        public function load_setting_page($page = NULL) {
            if(!$page) {
                show_404();
            }

            switch($page) {
                case 'account':
                    $data = array(
                        'user_data' => $this->user_info(true)
                    );

                    $result = array(
                        'html' => $this->_renderpage('pages/user/inside/widget/user/setting_account', $data, TRUE)
                    );
                    break;
                case 'security':
                    $result = array(
                        'html' => $this->_renderpage('pages/user/inside/widget/user/setting_security', NULL, TRUE)
                    );
                    break;
                case 'design':
                    $result = array(
                        'html' => $this->_renderpage('pages/user/inside/widget/user/setting_design', NULL, TRUE)
                    );
                    break;
            }

            $this->json_result($result);
        }

        public function edit_account() {
            $this->load->model('user_m');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $username = strtolower($this->input->post('username'));
            $current = $this->user_info(true);

            $data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'username' => $username
            );

            $refresh = false;
            if($current->username != $username) {
                if($this->user_m->check_user('users', array('username' => $username)) && $username != '' && strlen($username) >= 5) {
                    $data['username'] = $username;
                    $refresh = true;
                } else {
                    unset($data['username']);
                    $result = array(
                        'status' => false,
                        'message' => 'Nama pengguna telah digunakan',
                        'target' => 'username'
                    );

                    $this->json_result($result);

                    exit;
                }
            }

            $update = $this->db->update('users', $data, array('id' => $current->id));

            $result = array(
                'status' => $update ? true : false,
                'message' => $update ? 'Success' : 'Failed',
                'username' => $username,
                'refresh' => $refresh,
                'target' => false
            );
            $this->json_result($result);
        }

        public function edit_security() {
            $old_password = $this->input->post('current_password');
            $password = $this->input->post('password');
            $password_c = $this->input->post('password_c');
            $user_info = $this->user_info(true);

            if($this->ion_auth->hash_password_db($user_info->id, $old_password)) {
                if($password != '' || $password_c != '') {
                    if ($password == $password_c) {
                        $data = array(
                            'password' => $password
                        );

                        $update = $this->ion_auth->update($user_info->id, $data);

                        $result = array(
                            'status' => $update ? true : false,
                            'message' => $update ? 'Berhasil mengganti kata sandi' : 'Gagal menyimpan perubahan',
                            'target' => false
                        );
                    } else {
                        $result = array(
                            'status' => false,
                            'message' => 'Kata sandi tidak cocok',
                            'target' => 'password_c'
                        );
                    }
                } else {
                    $result = array(
                        'status' => false,
                        'message' => 'Kata sandi tidak boleh kosong',
                        'target' => 'password'
                    );
                }
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Kata sandi aktif yang anda masukan salah',
                    'target' => 'current_password'
                );
            }

            $this->json_result($result);
        }

        public function edit_design($type) {
            switch($type) {
                case 'profile':
                    $profile = $this->do_upload('profile_picture', './assets/uploads/users/profile_picture', 'jpg|jpeg|png');

                    if($profile) {
                        $data = array(
                            'name' => $profile['file_name'],
                            'type' => $profile['file_type'],
                            'size' => $profile['file_size'],
                            'path' => './assets/uploads/users/profile_picture/',
                            'full_path' => './assets/uploads/users/profile_picture/'.$profile['file_name']
                        );
                        $this->db->insert('image', $data);
                        $image_id = $this->db->insert_id();

                        $this->db->update('users', array('profile_picture' => $image_id), array('id' => $this->ion_auth->user()->row()->id));

                        $result = array(
                            'status' => true,
                            'message' => 'Success'
                        );
                    } else {
                        $result = array(
                            'status' => true,
                            'message' => 'Tidak ada perubahan'
                        );
                    }
                    break;
                case 'cover':
                    break;
            }

            $this->json_result($result);
        }

    }