<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class User extends MY_Controller {

        public function __construct() {
            parent::__construct();
        }

        public function index($username = null, $page = null) {
            $this->load->helper('global_helper');
            if(!$username) {
                redirect('');
            }

            $this->load->model('user_m');
            $user_profile = $this->user_m->get_user_profile($username);
            if(!$user_profile) {
                show_404();
            }

            $user_id = $this->_user('id');

            $data = array(
                'title' => $user_profile['first_name'],
                '_js' => array(
                    'assets/lib/ajaxform/jquery.ajaxform.min.js',
                    'assets/lib/stickyjs/jquery.sticky.js'
                ),
                'user_profile' => $user_profile,
                'user_info' => $this->_user(array('status')),
                'is_user_profile' => $user_profile['id'] == $user_id['id'] ? TRUE : FALSE,
                'page' => $page
            );
            if(!$page) {
                $data['content'] = 'pages/user/inside/user';
                $data['_js'][] = 'assets/pages/user/user.js?v='.time();
            } else {
                switch($page) {
                    case 'settings':
                        if(!$data['is_user_profile']) {
                            redirect('user/'.$username);
                        }

                        $data['content'] = 'pages/user/inside/user_setting';
                        $data['_js'][] = 'assets/pages/user/user_setting.js?v='.time();
                        break;
                    default:
                        redirect('user/'.$username);
                }
            }
            $this->_renderpage('theme/user/inside/index', $data);
        }

    }