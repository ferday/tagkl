<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pkl extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('pkl_model');
    }

    public function index() {
        $user_id = $this->_user('id');
        $pkl = $this->pkl_model->get_pkl(null, array('pkl.user_id' => $user_id['id']), null, TRUE);

        $account_status = array();
        if($pkl['status'] == 0) {
            $account_status = array(
                'status' => false,
                'message' => 'Akun anda belum aktif, anda belum bisa menggunakan TagKL sepenuhnya',
                'type' => 'danger'
            );
        } else if($pkl['status'] == 2) {
            $account_status = array(
                'status' => false,
                'message' => 'Akun anda tidak disetujui karena tidak sesuai dengan syarat dan ketentuan TagKL, akun anda akan dihapus 3 hari lagi',
                'type' => 'danger'
            );
        }
        $data = array(
            'title' => 'Beranda',
            'content' => 'pages/pkl/beranda',
            '_js' => array(
                'https://maps.google.com/maps/api/js?key=AIzaSyCkBchml0SXGT_rTjuyewdIMRxr4YrT73o',
                'assets/lib/ajaxform/jquery.ajaxform.min.js',
                'assets/pages/pkl/beranda.js?v='.time()
            ),
            'pkl' => $pkl,
            'account_status' => $account_status,
            'with_title' => true
        );
        $this->_renderpage('theme/pkl/index', $data);
    }

    public function profile() {
        $user_id = $this->_user('id');
        $pkl = $this->pkl_model->get_pkl(null, array('pkl.user_id' => $user_id['id']), null, TRUE);
        $data = array(
            'title' => 'Profil',
            'content' => 'pages/pkl/profile',
            '_js' => array(
                'assets/lib/ajaxform/jquery.ajaxform.min.js',
                'assets/pages/pkl/profile.js?v='.time()
            ),
            'pkl' => $pkl,
            'with_title' => false
        );
        $this->_renderpage('theme/pkl/index', $data);
    }
}