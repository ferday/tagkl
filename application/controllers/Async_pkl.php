<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Async_pkl extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('pkl_model');
    }

    public function getLocationName() {
        $lat = $this->input->get('lat');
        $lng = $this->input->get('lng');

        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng;

        $result = $this->_curl($url);

        $this->json_result($result);
    }

    public function check_user_location_default() {
        $id = $this->_user('id');

        $pkl = $this->pkl_model->get_pkl(null, array('pkl.user_id' => $id['id']), array('lat','lng'), true);

        if($pkl) {
            $result = array(
                'status' => true,
                'data' => $pkl
            );
        } else {
            $result = array(
                'status' => false,
                'data' => null
            );
        }

        $this->json_result($result);
    }

    public function save_default_position() {
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $id = $this->_user('id');

        $data = array(
            'lat' => $lat,
            'lng' => $lng
        );

        $update = $this->db->update('pkl', $data, array('user_id' => $id['id']));

        if($update) {
            $result = array(
                'message' => 'Success',
                'status' => true
            );
        } else {
            $result = array(
                'message' => 'Failed',
                'status' => false
            );
        }
        $this->json_result($result);
    }

    public function load_content($type = null) {
        switch ($type) {
            case 'activity':
                $result = array(
                    'html' => $this->_renderpage('pages/pkl/widget/profile/activity', null, true)
                );
                break;
            default:
                redirect('');
                break;
        }

        $this->json_result($result);
    }

}