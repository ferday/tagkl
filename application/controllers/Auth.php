<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('auth_m');
	}

	public function index() {
		if($this->ion_auth->logged_in()) {
			if($this->ion_auth->in_group(1)) {
				redirect('admin');
			} else {
				redirect(base_url());
			}
		}
		$data = array(
			'title' => 'Masuk ke TagKL',
			'content' => 'pages/user/auth/sign_in',
			'background' => $this->auth_m->get_background()->result_array(),
			'_js' => array(
				'assets/lib/ajaxform/jquery.ajaxform.min.js',
				'assets/pages/auth/auth.js?v='.time()
				)
			);
		$this->_renderpage('theme/user/auth/index', $data);
	}

	public function login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if($this->ion_auth->login($username,$password)) {
			if($this->ion_auth->in_group(1)) {
				$target = base_url().'admin/';
			} else {
				$target = base_url();
			}
			$result = array(
				'status' => true,
				'target' => $target
				);
		} else {
			$result = array(
				'status' => false
				);
		}
		$this->json_result($result);
	}

	public function logout() {
		$this->ion_auth->logout();
		redirect('auth');
	}

	public function signup_modal($type) {
		switch($type) {
			case 'pembeli':
				echo $this->_renderpage('pages/user/auth/widget/w_modal_signup_pembeli', null, true);
			break;
			case 'pkl':
				echo $this->_renderpage('pages/user/auth/widget/w_modal_signup_pkl', null, true);
			break;
		}
	}

	public function register() {
		$f_name = $this->input->post('f_name');
		$l_name = $this->input->post('l_name');
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($this->auth_m->check_user('users', array('username' => $username))) {
			if($this->auth_m->check_user('users', array('email' => $email))) {
				$data = array(
					'first_name' => $f_name,
	                'last_name' => $l_name,
	                'username' => $username,
	                'status' => 0,
					);
                $register = $this->ion_auth->register($username, $password, $email, $data);
				if($register) {
					$this->auth_m->mailing($register);
					$result = array(
						'status' => true,
						'message' => 'Success',
						'target' => false
						);
				} else {
					$result = array(
						'status' => false,
						'message' => 'Unknown error occured.',
						'target' => false
						);
				}
			} else {
				$result = array(
					'status' => false,
					'message' => 'Email sudah terpakai.',
					'target' => 'email'
					);
			}
		} else {
			$result = array(
				'status' => false,
				'message' => 'Nama Pengguna sudah terpakai.',
				'target' => 'username'
				);
		}

		$this->json_result($result);
	}

	public function pkl_register() {
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$product = $this->input->post('product');
		$email = $this->input->post('email');
		$address = $this->input->post('address');
		$postal_code = $this->input->post('postal_code');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $pkl_product = $this->do_upload('pkl_product', './assets/uploads/pkl/product/', 'jpg|jpeg|png|gif');

		if($this->auth_m->check_user('users', array('email' => $email))) {
		    if($this->auth_m->check_user('users', array('username' => $username))) {
                $data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'status' => 0,
                    'address' => $address,
                    'postal_code' => $postal_code
                );
                $user_id = $this->ion_auth->register($username, $password, $email, $data, array('3'));

                $img_id = null;
                if ($pkl_product) {
                    $file_data = array(
                        'name' => $pkl_product['file_name'],
                        'type' => $pkl_product['file_type'],
                        'extension' => $pkl_product['file_ext'],
                        'size' => $pkl_product['file_size'],
                        'path' => './assets/uploads/pkl/product/',
                        'full_path' => './assets/uploads/pkl/product/' . $pkl_product['file_name'],
                        'user_id' => $user_id
                    );
                    $this->db->insert('image', $file_data);
                    $img_id = $this->db->insert_id();
                }

                $data = array(
                    'product' => $product,
                    'status' => 0,
                    'user_id' => $user_id
                );
                if ($this->auth_m->insert('pkl', $data)) {
                    $this->ion_auth->update($user_id, array('profile_picture' => $img_id));
                    $result = array(
                        'status' => true,
                        'message' => 'Success',
                        'target' => false
                    );
                } else {
                    $result = array(
                        'status' => false,
                        'message' => 'Unknown Error Occured.',
                        'target' => false
                    );
                }
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Nama Peengguna sudah terpakai.',
                    'target' => 'username'
                );
            }
		} else {
			$result = array(
				'status' => false,
				'message' => 'Email sudah terpakai.',
				'target' => 'email'
				);
		}

		$this->json_result($result);
	}
}

?>