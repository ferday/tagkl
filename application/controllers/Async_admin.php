<?php

class Async_admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function manage_pkl_content($content) {
        $this->load->model('pkl_model');
        switch ($content) {
            case 'pkl_request':
                $request_list = $this->pkl_model->get_pkl(null, array('pkl.status' => 0));
                $data = array(
                    'new_request' => $request_list
                );
                $result = array(
                    'view' => $this->_renderpage('pages/admin/widget/manage_pkl/pkl_request', $data, true)
                );
                break;

            case 'pkl_accepted':
                $accepted_list = $this->pkl_model->get_pkl(null, array('pkl.status' => 1));
                $data = array(
                    'accepted_list' => $accepted_list
                );
                $result = array(
                    'view' => $this->_renderpage('pages/admin/widget/manage_pkl/pkl_accepted', $data, true)
                );
                break;

            case 'pkl_rejected':
                $rejected_list = $this->pkl_model->get_pkl(null, array('pkl.status' => 2));
                $data = array(
                    'rejected_list' => $rejected_list
                );
                $result = array(
                    'view' => $this->_renderpage('pages/admin/widget/manage_pkl/pkl_rejected', $data, true)
                );
                break;

            case 'pkl_active':
                $result = array(
                    'view' => $this->_renderpage('pages/admin/widget/manage_pkl/pkl_active', null, true)
                );
                break;
        }

        $this->json_result($result);
    }

    public function pkl_detail_modal() {
        $this->load->model('pkl_model');

        $pkl_id = $this->input->post('pkl_id');
        $detail = $this->pkl_model->get_pkl($pkl_id);
        $data = array(
            'pkl_detail' => $detail,
            'source' => $this->input->post('source')
        );
        echo $this->_renderpage('pages/admin/widget/manage_pkl/pkl_detail_modal', $data, true);
    }

    public function pkl_delete_modal() {
        $data = array(
            'pkl_id' => $this->input->post('pkl_id'),
            'name' => $this->input->post('name'),
            'content' => $this->input->post('content')
        );
        echo $this->_renderpage('pages/admin/widget/manage_pkl/pkl_delete_modal', $data, true);
    }

    public function delete_pkl() {
        $pkl_id = $this->input->post('pkl_id');
        $delete = $this->db->delete('pkl', array('id' => $pkl_id));
        if($delete) {
            $result = array(
                'status' => true,
                'message' => 'Successfully to remove PKL'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to remove PKL'
            );
        }
        $this->json_result($result);
    }

    public function pkl_block_modal() {
        $data = array(
            'pkl_id' => $this->input->post('pkl_id'),
            'name' => $this->input->post('name')
        );
        echo $this->_renderpage('pages/admin/widget/manage_pkl/pkl_block_modal', $data, true);
    }

    public function pkl_edit_modal() {
        $this->load->model('pkl_model');

        $id = $this->input->post('pkl_id');
        $detail = $this->pkl_model->get_pkl($id);
        $data = array(
            'detail' => $detail
        );
        echo $this->_renderpage('pages/admin/widget/manage_pkl/pkl_edit_modal', $data, true);
    }

    public function edit_pkl() {
        $this->load->model('pkl_model');

        $pkl_id = $this->input->post('pkl_id');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $product = $this->input->post('product');

        $data = array(
            'product' => $product
        );
        $this->db->update('pkl', $data, array('id' => $pkl_id));
        $user_id = $this->pkl_model->get_pkl($pkl_id, null, array('pkl.user_id'));
        $data_user = array(
            'first_name' => $first_name,
            'last_name' => $last_name
        );
        $edit = $this->ion_auth->update($user_id['user_id'], $data_user);
        if($edit) {
            $result = array(
                'status' => true,
                'message' => 'Successfully to edit PKL'
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed to edit PKL'
            );
        }
        $this->json_result($result);
    }

    public function pkl_accept_modal() {
        $data = array(
            'pkl_id' => $this->input->post('pkl_id'),
            'name' => $this->input->post('name')
        );
        echo $this->_renderpage('pages/admin/widget/manage_pkl/pkl_accept_modal', $data, true);
    }

    public function request_action() {
        $this->load->model('pkl_model');

        $id = $this->input->post('id');
        $type = $this->input->post('type');

        $action = $this->pkl_model->request_action($id,$type);

        if($action) {
            $result = array(
                'status' => true,
                'message' => 'Success',
                'id' => $action
            );
        } else {
            $result = array(
                'status' => false,
                'message' => 'Failed'
            );
        }

        $this->json_result($result);
    }

    public function refresh_stats() {
        $this->load->model('pkl_model');

        $data = array(
            'pkl-request' => $this->pkl_model->get_total(0),
            'pkl-accepted' => $this->pkl_model->get_total(1),
            'pkl-rejected' => $this->pkl_model->get_total(2)
        );

        $this->json_result($data);
    }
}