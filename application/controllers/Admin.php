<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct() {
		parent::__construct();
        if(php_sapi_name() != 'cli') {
            if (!$this->ion_auth->in_group(1)) {
                redirect(base_url());
            }
        }
		$this->load->model('admin_m');
	}

	public function index() {
		$data = array(
			'title' => 'Dashboard',
			'content' => 'pages/admin/dashboard',
			);
		$this->_renderpage('theme/admin/index', $data);
	}

    public function manage_pkl() {
        $this->load->model('pkl_model');
		$data = array(
			'title' => 'PKL Management',
			'content' => 'pages/admin/manage_pkl',
            '_css' => array(
                'assets/lib/datatable/css/dataTables.bootstrap.min.css'
            ),
            '_js' => array(
                'https://maps.google.com/maps/api/js?key=AIzaSyCkBchml0SXGT_rTjuyewdIMRxr4YrT73o',
                'assets/lib/gmaps/gmaps.js',
                'assets/lib/datatable/js/jquery.dataTables.min.js',
                'assets/lib/datatable/js/dataTables.bootstrap.min.js',
                'assets/lib/ajaxform/jquery.ajaxform.min.js',
                'assets/pages/admin/manage_pkl.js?v='.time()
            ),
            'data' => array(
                'total_request' => $this->pkl_model->get_total(0),
                'total_accepted' => $this->pkl_model->get_total(1),
                'total_rejected' => $this->pkl_model->get_total(2)
            )
			);
		$this->_renderpage('theme/admin/index', $data);
	}

    public function add_pkl() {
        $data = array(
            'title' => 'PKL Management',
            'content' => 'pages/admin/add_pkl',
            '_css' => array(
                'assets/lib/datatable/css/dataTables.bootstrap.min.css'
            ),
            '_js' => array(
                'assets/lib/datatable/js/jquery.dataTables.min.js',
                'assets/lib/datatable/js/dataTables.bootstrap.min.js',
                'assets/lib/ajaxform/jquery.ajaxform.min.js',
                'assets/pages/admin/add_pkl.js'
            ),
        );
        $this->_renderpage('theme/admin/index', $data);
    }

    public function create_pkl() {
        $product = $this->input->post('product');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $address = $this->input->post('address');
        $postal_code = $this->input->post('postal_code');
        $email = $this->input->post('email');
        $generate = $this->input->post('generate');

        $data = array(
            'product' => $product,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'address' => $address,
            'postal_code' => $postal_code,
            'email' => $email,
            'status' => 1
        );

        if($this->admin_m->check_user('pkl', array('email' => $email)) || $this->admin_m->check_user('pkl', array('email' => $email))) {
            $insert = $this->db->insert('pkl', $data);
            if($insert) {
                if($generate) {
                    $generate = $this->admin_m->generate_user($data);
                    $data_mailing = array(
                        'subject' => 'TagKL Account Information',
                        'email_to' => $data['email'],
                        'body' => 'Selamat! Username: ' . $generate['username'] . ' Password: ' . $generate['password']
                    );
                    $this->admin_m->mailing_log($data_mailing);

                    $result = array(
                        'status' => true,
                        'message' => 'Successfully create new PKL.'
                    );
                } else {
                    $username = $this->input->post('username');
                    $password = $this->input->post('password');

                    $data_user = array(
                        'first_name' => $data['first_name'],
                        'last_name' => $data['last_name'],
                        'username' => $username
                    );

                    if($this->admin_m->check_user('users', array('username' => $username))) {
                        $user_id = $this->ion_auth->register($username, $password, $data['email'], $data_user);
                        $this->ion_auth->add_to_group(3, $user_id);

                        $data_mailing = array(
                            'subject' => 'TagKL Account Information',
                            'email_to' => $data['email'],
                            'body' => 'Selamat! Username: ' . $username . ' Password: ' . $password
                        );
                        $this->admin_m->mailing_log($data_mailing);

                        $result = array(
                            'status' => true,
                            'message' => 'Successfully create new PKL.'
                        );
                    } else {
                        $this->db->delete('pkl', array('email' => $data['email']));
                        $result = array(
                            'status' => false,
                            'message' => 'Username already exists',
                            'target' => 'username'
                        );
                    }
                }
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Failed to create new PKL',
                    'target' => false
                );
            }
        } else {
            $result = array(
                'status' => false,
                'message' => 'Email already exists',
                'target' => 'email'
            );
        }
        $this->json_result($result);
    }

    public function send_mail_to_pkl() {
        if(php_sapi_name() != 'cli') {
            redirect('admin');
        }
        $this->db->select('id');
        $this->db->where('status', 0);
        $mailing_id = $this->db->get('mailing_log')->result_array();
        foreach($mailing_id as $v) {
            $send = $this->mailing($v['id']);
            echo $send;
        }
    }
}

?>