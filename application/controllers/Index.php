<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->ion_auth->logged_in()) {
			redirect('auth');
		}
		if($this->ion_auth->in_group(1)) {
			redirect('admin');
		}
		if($this->ion_auth->in_group(3)) {
		    redirect('pkl');
        }
	}

	public function index() {
		if($this->ion_auth->user()->row()->status == 0) {
			$this->notification('alert', '<strong>Akunmu Belum Sepenuhnya Aktif.</strong> Silahkan cek email untuk mengatifkan akunmu. Tidak menerima email ? <a href="#" class="alert-link">Kirim Ulang</a>.');
		}

		$hour = date("G", time());

        if ($hour>=0 && $hour<=11) {
            $current_date = "Pagi";
        }
        elseif ($hour >=12 && $hour<=14) {
            $current_date = "Siang";
        }
        elseif ($hour >=15 && $hour<=17) {
            $current_date = "Sore";
        }
        elseif ($hour >=17 && $hour<=18) {
            $current_date = "Petang";
        }
        elseif ($hour >=19 && $hour<=23) {
            $current_date = "Malam";
        }
		$data = array(
			'title' => 'TagKL',
			'content' => 'pages/user/inside/beranda',
            '_css' => array(
                'assets/lib/owl-carousel/css/owl.carousel.css',
                'assets/lib/owl-carousel/css/owl.theme.css'
            ),
			'_js' => array(
				'https://maps.google.com/maps/api/js?key=AIzaSyCkBchml0SXGT_rTjuyewdIMRxr4YrT73o',
				'assets/lib/gmaps/gmaps.js',
                'assets/lib/stickyjs/jquery.sticky.js',
                'assets/lib/owl-carousel/js/owl.carousel.min.js',
				'assets/pages/user/beranda.js?v='.time()
				),
            'user_info' => $this->_user(array('status')),
            'current_date' => $current_date
			);
		$this->_renderpage('theme/user/inside/index', $data);
	}

	public function widget_pkl_new() {
	    $this->load->model('pkl_model');
	    $new_pkl_list = $this->pkl_model->get_pkl();
        $data = array(
            'new_pkl' => $new_pkl_list
        );
		$result = array(
			'view' => $this->_renderpage('pages/user/inside/widget/beranda/pkl_new', $data, true)
        );
		$this->json_result($result);
	}

	public function widget_pkl_ads() {
	    $this->load->model('pkl_model');

        $data = array();

        $result = array(
            'view' => $this->_renderpage('pages/user/inside/widget/beranda/pkl_ads', $data, true)
        );
        $this->json_result($result);
    }

    public function news_feed() {
        $offset = $this->input->post('offset');

        $result = array(
            'status' => true,
            'view' => $this->_renderpage('pages/user/inside/widget/beranda/news_feed', null, true)
        );
        $this->json_result($result);
    }

    public function news_feed_side_ads() {
        $result = array(
            'status' => true,
            'view' => $this->_renderpage('pages/user/inside/widget/beranda/news_feed_side_ads', null, true)
        );
        $this->json_result($result);
    }

}

?>