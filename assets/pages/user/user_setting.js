$(function() {
    var loading = '<div style="padding-top:100px;"><div class="loader" style="margin-top: 0;">Loading...</div></div>';
    $.fn.load_setting_page = function(page) {
        $.ajax({
            url: site_url + 'async/load_setting_page/'+page,
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                $('#setting-container').html(loading);
            },
            success: function(r) {
                $('#setting-container').html(r.html);
            },
            error: function() {
                $('#setting-container').html('<h3 class="text-center no-margin mdc-text-grey-600" style="padding-top: 100px; line-height: 1.5;"><i class="zmdi zmdi-mood-bad zmdi-hc-lg"></i> <br> Failed to load content</h3>');
                $(this).notification('error', 'Failed to load content');
            }
        });
    };

    $(this).load_setting_page('account');

    $(this).on('click', '.nav-setting li a', function(e) {
        if(!$(this).parent().hasClass('active')) {
            $(this).load_setting_page($(this).data('to'));
            $('.nav-setting li').removeClass('active');
            $(this).parent().addClass('active');
        }
    });

    $(this).on('submit', '#form-edit-account', function(e) {
        var button = $(this).find('button[type="submit"]');

        $(this).ajaxSubmit({
            url: site_url + 'async/edit_account',
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                NProgress.start();
                button.attr('disabled', 'disabled');
                button.html('<i class="zmdi zmdi-chart-donut zmdi-hc-spin"></i> Sedang Menyimpan...');
            },
            success: function(r) {
                NProgress.done();
                button.removeAttr('disabled');
                button.html('<i class="zmdi zmdi-check"></i> Simpan');

                if(r.status) {
                    $(this).notification('success', 'Berhasil memperbaharui info akun');

                    if(r.refresh) {
                        window.history.pushState(null, null, base_url + "user/" + r.username + '/settings');
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                } else {
                    $(this).notification('error', r.message);
                    $('#form-edit-account').find('input[name="'+r.target+'"]').parent().addClass('has-error');
                    $('#form-edit-account').find('input[name="'+r.target+'"]').focus();
                }
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Gagal menyimpan perubahan');
                button.removeAttr('disabled');
                button.html('<i class="zmdi zmdi-check"></i> Simpan');
            }
        });

        e.preventDefault();
    });

    $(this).on('submit', '#form-edit-security', function(e) {
        var button = $(this).find('button[type="submit"]');

        $(this).ajaxSubmit({
            url: site_url + 'async/edit_security',
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                NProgress.start();
                button.attr('disabled', 'disabled');
                button.html('<i class="zmdi zmdi-chart-donut zmdi-hc-spin"></i> Sedang Menyimpan...');
            },
            success: function(r) {
                NProgress.done();
                button.removeAttr('disabled');
                button.html('<i class="zmdi zmdi-check"></i> Simpan');

                if(r.status) {
                    $(this).notification('success', r.message);
                    $('#form-edit-security').trigger('reset');
                } else {
                    $(this).notification('error', r.message)

                    $('#form-edit-security').find('input[name="'+r.target+'"]').parent().addClass('has-error');
                    $('#form-edit-security').find('input[name="'+r.target+'"]').focus();
                }
            },
            error: function() {
                NProgress.done();
                button.removeAttr('disabled');
                button.html('<i class="zmdi zmdi-check"></i> Simpan');

                $(this).notification('error', 'Gagal menyimpan perubahan');
            }
        });

        e.preventDefault();
    });

    $(this).on('submit', '#edit-design-profile-form', function(e) {
        var button = $(this).find('button[type="submit"]');
        $(this).ajaxSubmit({
            url: site_url + 'async/edit_design/profile/',
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                NProgress.start();
                button.attr('disabled', 'disabled');
                button.html('Menyimpan...');
            },
            success: function(r) {
                NProgress.done();
                button.removeAttr('disabled');
                button.html('Simpan Foto Profil');

                if(r.status) {
                    $(this).notification('success', r.message);
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                } else {
                    $(this).notification('error', r.message);
                }
            },
            error: function() {
                NProgress.done();
                button.removeAttr('disabled');
                button.html('Simpan Foto Profil');
            }
        });
        e.preventDefault();
    });

    $(this).on('change', ".fileUpload .upload", function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.edit-profile-picture-container .img-preview').show();
                $('.edit-profile-picture-container h3').hide();
                $('.edit-profile-picture-container .img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

});