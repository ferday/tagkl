$(function() {
    var loading = '<div class="loader">Loading...</div>';
    var page = 0;
    var reached_end;
    $.fn.get_status = function(page) {
        $.ajax({
            url: site_url + 'async/get_status',
            type: 'POST',
            data: {
                user_id: $('#user_id').val(),
                page: page
            },
            dataType: 'JSON',
            beforeSend: function() {
                $('#status-list-wrap').append(loading);
            },
            success: function(r) {
                $('#status-list-wrap').find('.loader').remove();
                $.each(r.data, function(k, v) {
                    var status_body = '<div class="tagkl-box news-feed-box animated fadeIn">';
                    status_body += '<div class="news-feed-header">';
                    status_body += '<a href="#">';
                    status_body += '<div class="row">';
                    status_body += '<div class="col-md-2" style="text-align: center;">';
                    status_body += '<img src="'+base_url+v.profile_picture+'" class="img-circle" alt="Profile Picture">';
                    status_body += '</div>';
                    status_body += '<div class="col-md-7" style="padding-left: 0;">';
                    status_body += '<p class="mdc-text-light-blue-700">';
                    status_body += v.first_name + ' ' + v.last_name;
                    status_body += '<br>';
                    status_body += '<small>'+ v.time_ago +'</small>';
                    status_body += '</p>';
                    status_body += '</div>';
                    status_body += '<div class="col-md-3 news-feed-reaction">';
                    status_body += '<div class="btn-group no-margin no-padding">';
                    status_body += '<a href="#" data-target="#" class="dropdown-toggle mdc-text-grey-500" data-toggle="dropdown">';
                    status_body += '<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>';
                    status_body += '</a>';
                    status_body += '<ul class="dropdown-menu dropdown-menu-right">';
                    if(v.yours) {
                        status_body += '<li><a class="remove-status" href="#" data-id="' + v.id + '">Hapus</a></li>';
                    } else {
                        status_body += '<li><a href="#">Laporkan</a></li>';
                    }
                    status_body += '</ul>';
                    status_body += '</div>';
                    status_body += '</div>';
                    status_body += '</div>';
                    status_body += '</a>';
                    status_body += '</div>';
                    status_body += '<br>';
                    status_body += '<div class="news-feed-body">';
                    var style = v.description.length < 30 ? 'font-size: 30px;' : '';
                    status_body += '<p style="'+style+'">';
                    status_body += v.description;
                    status_body += '</p>';
                    if(v.hasOwnProperty('image')) {
                        status_body += '<img src="'+base_url+v.image.full_path+'" class="img-responsive">';
                    }
                    status_body += '<div class="like-count pull-right">';
                    status_body += '<a href="#" class="mdc-text-blue-grey-700 like-detail" data-id="' + v.id + '"><i class="zmdi zmdi-mood zmdi-hc-fw zmdi-hc-lg"></i> <span>' + v.like + '</span></a>';
                    status_body += '</div><br>';
                    status_body += '<hr style="margin-bottom: 0;">';
                    status_body += '<div class="news-feed-reaction">';
                    var liked = v.liked_by_me ? 'liked' : '';
                    status_body += '<a href="#" class="mdc-text-grey-500 '+liked+'" data-id="'+v.id+'" data-type="like"><i class="zmdi zmdi-mood zmdi-hc-lg zmdi-hc-fw"></i> Suka</a>';
                    status_body += '</div>';
                    status_body += '</div>';

                    $('#status-list-wrap').append(status_body);
                });

                if(jQuery.isEmptyObject(r.data) && $('#status-list-wrap').find('p.end').length < 1) {
                    reached_end = true;
                    if(page > 1) {
                        $('#status-list-wrap').append('<br><p class="text-center mdc-text-grey-400 end">Sudah mencapai akhir. <a href="#">Ke atas <i class="zmdi zmdi-chevron-up"></i></a></p><br>');
                    }
                    else {
                        $('#status-list-wrap').append('<br><p class="text-center mdc-text-grey-400 end">Tidak ada aktifitas</p><br>');
                    }
                }
                $('[data-toggle="tooltip"]').tooltip();
            },
            error: function() {
                $('#status-list-wrap').html('Error');
            }
        });
    };

    $(this).get_status();

    $(this).on('submit', '#update-status-form', function(e) {
        if(!$(this).find('textarea[name="description"]').val()) {
            $(this).notification('error', 'Status tidak boleh kosong');
            return false;
        }
        $(this).ajaxSubmit({
            url: site_url + 'async/update_status',
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                NProgress.start();
                $('#update-status-form').find('button[type="submit"]').html('Loading...');
                $('#update-status-form').find('button[type="submit"]').attr('disabled', 'disabled');
            },
            success: function(r) {
                NProgress.done();
                $('#update-status-form').find('button[type="submit"]').html('<i class="zmdi zmdi-mail-send"></i> Perbaharui');
                $('#update-status-form').find('button[type="submit"]').removeAttr('disabled');

                if(r.status) {
                    $('#update-status-form').trigger('reset');
                    $.each(r.data, function(k, v) {
                        var status_body = '<div class="tagkl-box news-feed-box animated fadeIn">';
                            status_body += '<div class="news-feed-header">';
                            status_body += '<a href="#">';
                            status_body += '<div class="row">';
                            status_body += '<div class="col-md-2" style="text-align: center;">';
                            status_body += '<img src="'+base_url+v.profile_picture+'" class="img-circle" alt="Profile Picture">';
                            status_body += '</div>';
                            status_body += '<div class="col-md-7" style="padding-left: 0;">';
                            status_body += '<p class="mdc-text-light-blue-700">';
                            status_body += v.first_name + ' ' + v.last_name;
                            status_body += '<br>';
                            status_body += '<small>'+ v.time_ago +'</small>';
                            status_body += '</p>';
                            status_body += '</div>';
                            status_body += '<div class="col-md-3 news-feed-reaction">';
                            status_body += '<a href="#">';
                            status_body += '<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>';
                            status_body += '</a>';
                            status_body += '</div>';
                            status_body += '</div>';
                            status_body += '</a>';
                            status_body += '</div>';
                            status_body += '<br>';
                            status_body += '<div class="news-feed-body">';
                            var style = v.description.length < 30 ? 'font-size: 30px;' : '';
                            status_body += '<p style="'+style+'">';
                            status_body += v.description;
                            status_body += '</p>';
                            if(v.hasOwnProperty('image')) {
                                status_body += '<img src="'+base_url+v.image.full_path+'" class="img-responsive">';
                            }
                            status_body += '<div class="like-count pull-right">';
                            status_body += '<a href="#" class="mdc-text-blue-grey-700"><i class="zmdi zmdi-mood zmdi-hc-fw zmdi-hc-lg"></i> <span>' + v.like + '</span></a>';
                            status_body += '</div><br>';
                            status_body += '<hr style="margin-bottom: 0;">';
                            status_body += '<div class="news-feed-reaction">';
                            var liked = v.liked_by_me ? 'liked' : '';
                            status_body += '<a href="#" class="mdc-text-grey-500 '+liked+'" data-id="'+v.id+'" data-type="like"><i class="zmdi zmdi-mood zmdi-hc-lg zmdi-hc-fw"></i> Suka</a>';
                            status_body += '<a href="#" class="mdc-text-grey-500" data-id="'+v.id+'" data-type="comment"><i class="zmdi zmdi-comment-outline zmdi-hc-lg zmdi-hc-fw"></i> Komentar</a>';
                            status_body += '</div>';
                            status_body += '</div>';

                        $('#status-list-wrap').prepend(status_body);
                    });
                    $('.image-container').hide();
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    $(this).notification('error', 'Gagal memperbaharui status');
                }
            },
            error: function() {
                NProgress.done();
                $('#update-status-form').find('button[type="submit"]').html('<i class="zmdi zmdi-mail-send"></i> Perbaharui');
                $('#update-status-form').find('button[type="submit"]').removeAttr('disabled');
                $(this).notification('error', 'Gagal memperbaharui status');
            }
        });

        e.preventDefault();
    });

    $(".fileUpload .upload").change(function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.image-container').show();
                $('.img-preview').attr('src', e.target.result);
                $('.image-container .after p').css('line-height', $('.img-preview').css('height'));
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    $(this).on('click', ".image-container .after p", function() {
        $('.image-container').hide();
        $('.fileUpload .upload').val('');
    });

    $(this).on('click', '.news-feed-body .news-feed-reaction a', function(e) {
        var id = $(this).data('id');
        var type = $(this).data('type');
        var element = $(this);
        var like_count_container = element.parent().parent().find('.like-count a span');
        var like_count = parseInt(like_count_container.html());

        element.find('i').removeClass('animated jello');
        element.toggleClass('liked');
        $.ajax({
            url: site_url + 'async/status_action/'+type,
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(r) {
                switch (type) {
                    case 'like':
                        element.find('i').addClass('animated jello');
                        switch(r.type) {
                            case 'like':
                                like_count_container.html(like_count+1);
                                break;
                            case 'unlike':
                                like_count_container.html(like_count-1);
                                break;
                        }
                        break;
                    case 'comment':
                        break;
                    default:
                        break;
                }
            },
            error: function() {

            }
        });

        e.preventDefault();
    });

    $(this).on('click', '.like-detail', function(e) {
        var id = $(this).data('id');
        $('#like-detail').modal('show');
        $.ajax({
            url: site_url + 'async/status_action/like_detail',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            beforeSend: function() {
                $('#like-detail .modal-body').html(loading);
            },
            success: function(r) {
                var modal_body = '';
                if(r.like.length > 0) {
                    $.each(r.like, function (k, v) {
                        modal_body += '<div class="row">';
                        modal_body += '<a href="' + site_url + 'user/' + v.username + '">';
                        modal_body += '<div class="col-md-12 like-list">';
                        modal_body += '<p class="mdc-text-grey-600"><img src="'+base_url+v.profile_picture+'" class="img-circle">';
                        modal_body += v.first_name + ' ' + v.last_name + '</p>';
                        modal_body += '</div>';
                        modal_body += '</a>';
                        modal_body += '</div>';
                    });
                } else {
                    modal_body += '<h5 class="text-center mdc-text-grey-500"><i class="zmdi zmdi-mood-bad zmdi-hc-2x"></i><br><br>Tidak ada yang menyukai kiriman ini</h5>';
                }

                $('#like-detail .modal-body').html(modal_body);
            },
            error: function() {
            }
        });

        e.preventDefault();
    });

    $(this).on('click', '.remove-status', function(e) {
        var id = $(this).data('id');
        var modal_body = '<p class="text-center mdc-text-grey-600">Apakah anda yakin ingin menghapus kiriman ini ?</p>';
        var modal_footer = '<button class="btn btn-default" data-dismiss="modal">Batal</button>';
        modal_footer += '<button class="btn btn-danger remove-status-btn" data-id="'+id+'"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Hapus Kiriman</button>';

        $('#remove-status').modal('show');
        $('#remove-status .modal-body').html(modal_body);
        $('#remove-status .modal-footer').html(modal_footer);

        e.preventDefault();
    });

    $(this).on('click', '.remove-status-btn', function(e) {
        var id = $(this).data('id');

        $('#remove-status').modal('hide');
        $.ajax({
            url: site_url + 'async/status_action/remove',
            type: 'POST',
            data: {
                id: id
            },
            dataType: 'JSON',
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                if(r.status) {
                    $('.remove-status[data-id="' + r.id + '"]').parent().parent().parent().parent().parent().parent().parent().removeClass('animated fadeIn');
                    $('.remove-status[data-id="' + r.id + '"]').parent().parent().parent().parent().parent().parent().parent().fadeOut();

                    $(this).notification('success', 'Berhasil dihapus');
                } else {
                    $(this).notification('error', 'Gagal menghapus status');
                }
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Gagal menghapus status');
            }
        });

        e.preventDefault();
    });

    $(window).scroll(function() {
        if($(window).scrollTop() == $(document).height() - $(window).height()) {
            if(!reached_end) {
                page++;
                $(this).get_status(page);
            }
        }
    });

    $(".tagkl-box.in-profile.liked-pkl").sticky({topSpacing:90});

});