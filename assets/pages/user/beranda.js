$(function() {
	$.fn.maps = function(lat,lng) {
		var map = new GMaps({
	      el: '#maps-box',
	      lat: lat,
	      lng: lng
	    });

	    map.addMarker({
		  lat: lat,
		  lng: lng,
		  title: 'Lima',
		  click: function(e) {
		    alert('You clicked in this marker');
		  }
		});
	};

	$.fn.pkl_new = function() {
		$.ajax({
			url: site_url + '/index/widget_pkl_new',
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				$('#pkl-new').html('Loading...');
			},
			success: function(r) {
				$('#pkl-new').html(r.view);
				$("#new-pkl-list").owlCarousel({
					autoPlay: 5000,
					items : 3,
					pagination: false
				});
			},
			error: function() {
				$('#pkl-new').html('error');
			}
		});
	};

	$.fn.pkl_ads = function() {
		$.ajax({
			url: site_url + '/index/widget_pkl_ads',
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				$('#pkl-ads').html('Loading...');
			},
			success: function(r) {
				$('#pkl-ads').html(r.view);
				$("#pkl-ads-list").owlCarousel({
					autoPlay: 10000,
					singleItem: true
				});
			},
			error: function() {
				$('#pkl-ads').html('error');
			}
		});
	};

	$.fn.news_feed = function(offset) {
	    $.ajax({
	        url: site_url + 'index/news_feed',
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                $('#news-feed').append('<div id="news-feed-loading">Loading...</div>');
            },
            success: function (r) {
                if(r) {
                    $('#news-feed').find('#news-feed-loading').remove();
                    $('#news-feed').append(r.view);

                    $('[data-toggle="tooltip"]').tooltip();
                }
            },
            error: function() {
                $('#news-feed').find('#news-feed-loading').remove();
                $('#news-feed').append('<div id="news-feed-error">Error</div>');
            }
        });
    };

    $.fn.news_feed_side_ads = function() {
    	$.ajax({
			url: site_url + 'index/news_feed_side_ads',
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				$('#news-feed-side-ads').append('<div id="news-feed-side-ads-loading">Loading...</div>');
			},
			success: function (r) {
				if(r) {
					$('#news-feed-side-ads').find('#news-feed-side-ads-loading').remove();
					$('#news-feed-side-ads').append(r.view);
				}
			},
			error: function() {
				$('#news-feed-side-ads').find('#news-feed-side-ads-loading').remove();
				$('#news-feed-side-ads').append('<div id="news-feed-side-ads-error">Error</div>');
			}
		});
	}

	function showPosition(position) {
		console.log(position);
		$(this).maps(position.coords.latitude, position.coords.longitude);
	}

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	} else {
		alert('Geolocation is not supported in your browser');
	}

	function readURL(input) {

	}

	$(".fileUpload .upload").change(function(){
		if (this.files && this.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('.image-container').show();
				$('.img-preview').attr('src', e.target.result);
				$('.image-container .after p').css('line-height', $('.img-preview').css('height'));
			}

			reader.readAsDataURL(this.files[0]);
		}
	});

	$(this).on('click', ".image-container .after p", function() {
		$('.image-container').hide();
		$('.fileUpload .upload').val('');
	});

	$(this).pkl_new();
	$(this).pkl_ads();
    $(this).news_feed();
	$(this).news_feed_side_ads();

	$("#right-side-box").sticky({topSpacing:90});
});