setInterval(get_pkl_position, 30000); //300000 MS == 5 minutes

function get_pkl_position() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(set_pkl_position);
  }
  else {
    alert('This browser not supported to get location');
  }
}
function set_pkl_position(position) {
  var lng = position.coords.longitude;
  var lat = position.coords.latitude;
  $.ajax({
    url: site_url + 'maps/set_pkl_location',
    type: 'POST',
    data: {
      lng: lng,
      lat: lat
    },
    beforeSend: function() {
      $.snackbar({content: 'Loading...'});
    },
    success: function() {
      $.snackbar({content: 'Success'});
    }
  });
}
