$(function () {
    var profile_content = $('#profile-content');

    $.fn.drawContent = function(data, type) {

    };

    $.fn.loadContent = function(target) {
        $.ajax({
            url: site_url + 'async_pkl/load_content/'+target,
            type: 'GET',
            dataType: 'JSON',
            beforeSend: function() {
                profile_content.html(loading);
            },
            success: function(r) {
                $('#profile-content').html(r.html);
            },
            error: function() {

            }
        });
    };

    $(this).loadContent('activity');

    $(this).on('click', '.profile-box-body .tab-container .tab', function(e) {
        var target = $(this).data('target');

        $('.tab').removeClass('active');
        $(this).addClass('active');

        $(this).loadContent(target);
    });
});