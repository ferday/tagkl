$(function() {
    var latlng;

    $(".ripple-effect").click(function(e){
        var rippler = $(this);

        // create .ink element if it doesn't exist
        if(rippler.find(".ink").length == 0) {
            rippler.append("<span class='ink'></span>");
        }

        var ink = rippler.find(".ink");

        // prevent quick double clicks
        ink.removeClass("animate");

        // set .ink diametr
        if(!ink.height() && !ink.width())
        {
            var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
            ink.css({height: d, width: d});
        }

        // get click coordinates
        var x = e.pageX - rippler.offset().left - ink.width()/2;
        var y = e.pageY - rippler.offset().top - ink.height()/2;

        // set .ink position and add class .animate
        ink.css({
            top: y+'px',
            left:x+'px'
        }).addClass("animate");
    });

    $.fn.notification = function(type, message) {
        Command: toastr[type](message)

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    };

    $(this).on('click', '.toggle-menu', function(e) {
        $("#wrapper").toggleClass("toggled");
        e.preventDefault();
    });

    $.fn.getLocationError = function(error) {
        alert(error.code);
    };

    $.fn.checkUserLocationDefault = function() {
        $.ajax({
            url: site_url + '/async_pkl/check_user_location_default',
            type: 'GET',
            dataType: 'JSON',
            success: function(r) {
                if(r.status) {
                    if(!r.data.lat || !r.data.lng) {
                        $('#modal-set-default-location').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            },
            error: function() {
                window.location.reload();
            }
        });
    };

    $(this).checkUserLocationDefault();

    $.fn.set_location_default_by_map = function(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        var map = new GMaps({
            div: '#set-location-container-map',
            lat: lat,
            lng: lng,
            zoom: 16
        });
        var marker = map.addMarker({
            lat: lat,
            lng: lng,
            draggable: true
        });
        var $form = $('#set-location-container #save-location-form');

        $form.find('[name="lat"]').val(marker.getPosition().lat());
        $form.find('[name="lng"]').val(marker.getPosition().lng());

        google.maps.event.addListener(marker, 'dragend', function(e){
            $form.find('[name="lat"]').val(e.latLng.lat());
            $form.find('[name="lng"]').val(e.latLng.lng());
        });
    };

    $(this).on('click', '#set-location-container .map-btn', function(e) {
        $('#set-location-container .before').addClass('hidden');
        $('#set-location-container #set-location-manual-form').addClass('hidden');
        $('#set-location-container .choose-location-by-map-container').removeClass('hidden');

        $.geolocation.get({win: $(this).set_location_default_by_map, fail: $(this).getLocationError});
    });

    $(this).on('click', '#set-location-container .manual-btn', function(e) {
        $('#set-location-container .before').addClass('hidden');
        $('#set-location-container #set-location-manual-form').removeClass('hidden');
    });

    $(this).on('submit', '#set-location-container #save-location-form', function(e) {
        var el = $(this);
        $(this).ajaxSubmit({
            url: site_url + 'async_pkl/save_default_position',
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                el.find('[type="submit"]').attr('disabled', 'disabled');
                el.find('[type="submit"]').html('<i class="zmdi zmdi-spinner zmdi-hc-spin"></i> Menyimpan');
            },
            success: function(r) {
                el.find('[type="submit"]').removeAttr('disabled');
                el.find('[type="submit"]').html('<i class="zmdi zmdi-check"></i> Simpan');

                if(r.status) {
                    $(this).notification('success', 'Sukses menyetel lokasi default');
                    $('#modal-set-default-location').modal('hide');
                } else {
                    $(this).notification('error', 'Gagal menyetel lokasi default');
                }
            },
            error: function () {
                el.find('[type="submit"]').removeAttr('disabled');
                el.find('[type="submit"]').html('<i class="zmdi zmdi-check"></i> Simpan');
            }
        });
        e.preventDefault();
    });
});
