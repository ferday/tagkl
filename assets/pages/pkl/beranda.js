$(function() {

    $.fn.currentPositionMap = function(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        var geo = null;

        $.ajax({
            url: site_url + 'async_pkl/getLocationName',
            type: 'GET',
            data: {
                lat: lat,
                lng: lng
            },
            dataType: 'JSON',
            success: function(r) {
                if(r.status == 'OK') {
                    var map = new GMaps({
                        div: '#currentPositionMap',
                        lat: lat,
                        lng: lng,
                        zoom: 17
                    });

                    map.addMarker({
                        lat: lat,
                        lng: lng,
                        title: r.results[2]['formatted_address'],
                        infoWindow: {
                            content: '<p>'+ r.results[2]['formatted_address'] +'</p>'
                        }
                    });
                } else {
                    $(this).notification('error', 'Terjadi kesalahan mengambil lokasi');
                }
            },
            error: function() {
                $(this).notification('error', 'Terjadi kesalahan mengambil lokasi');
            }
        });
    };

    $.geolocation.get({win: $(this).currentPositionMap, fail: $(this).getLocationError});
});
