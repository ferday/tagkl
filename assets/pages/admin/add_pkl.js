$(function() {
    var loading = '<div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';

    $(this).on('submit', '#form-create-pkl', function(e) {
        $(this).ajaxSubmit({
            url: site_url + 'admin/create_pkl',
            type: 'POST',
            beforeSend: function() {
                NProgress.start();
                $('#form-create-pkl').find('button[type="submit"]').html('Loading...');
                $('#form-create-pkl').find('button[type="submit"]').attr('disabled', 'disabled');
            },
            success: function(r) {
                NProgress.done();
                $('#form-create-pkl').find('button[type="submit"]').html('Save');
                $('#form-create-pkl').find('button[type="submit"]').removeAttr('disabled');
                if(r.status) {
                    $(this).notification('success', r.message);
                    $('#form-create-pkl').trigger('reset');
                    $('#form-create-pkl').find('input').removeAttr('disabled');
                    $('#form-create-pkl').find('input').attr('required', 'required');
                } else {
                    $(this).notification('error', r.message);
                    if(r.target) {
                        $('#form-create-pkl').find('input[name="' + r.target + '"]').parent().addClass('has-error');
                    }
                }
            },
            error: function() {
                NProgress.done();
                $('#form-create-pkl').find('button[type="submit"]').html('Save');
                $('#form-create-pkl').find('button[type="submit"]').removeAttr('disabled');
                $(this).notification('error', 'Connection Time Out');
            }
        });

        e.preventDefault();
    });

    $(this).on('click', 'input[name="generate"]', function() {
        if($(this).is(':checked')) {
            $('#form-create-pkl').find('input[name="username"]').attr('disabled', 'disabled');
            $('#form-create-pkl').find('input[name="password"]').attr('disabled', 'disabled');
            $('#form-create-pkl').find('input[name="username"]').removeAttr('required');
            $('#form-create-pkl').find('input[name="password"]').removeAttr('required');
        } else {
            $('#form-create-pkl').find('input[name="username"]').removeAttr('disabled');
            $('#form-create-pkl').find('input[name="password"]').removeAttr('disabled');
            $('#form-create-pkl').find('input[name="username"]').attr('required', 'required');
            $('#form-create-pkl').find('input[name="password"]').attr('required', 'required');
        }
    })

});