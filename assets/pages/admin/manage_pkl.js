$(function() {
    var loading = '<div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';

    $.fn.refresh_stats = function() {
        $.ajax({
            url: site_url + 'async_admin/refresh_stats/',
            type: 'POST',
            success: function(r) {
                $.each(r, function(k, v) {
                    $('#'+k).find('h3').html(v);
                });
            }
        });
    };

    $.fn.manage_pkl_content = function(content) {
        $.ajax({
            url: site_url + 'async_admin/manage_pkl_content/'+content,
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function() {
                NProgress.start();
                $('#manage-pkl-content').html(loading);
            },
            success: function(r) {
                NProgress.done();
                $('#manage-pkl-content').html(r.view);
            },
            error: function() {
                NProgress.done();
                $('#manage-pkl-content').html('Failed to load content.');
            }
        });
    };
    $(this).manage_pkl_content('pkl_request');

    $.fn.request_action = function(id, type, refresh) {
        $.ajax({
            url: site_url + 'async_admin/request_action/',
            type: 'POST',
            data: {
                id: id,
                type: type
            },
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                if(r.status) {
                    $(this).notification('success', r.message);
                    $('.modal').modal('hide');
                    $('.pkl-list[data-id="'+r.id+'"]').remove();
                    $(this).refresh_stats();

                    if(refresh) {
                        $(this).manage_pkl_content(refresh);
                    }
                }
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Failed');
            }
        });
    };

    $(this).on('click', '.request-action', function(e) {
        var id = $(this).data('id');
        var type = $(this).data('type');

        $(this).request_action(id, type);
        e.preventDefault();
    });

    $(this).on('click', '.btn-change-content', function() {
        var content = $(this).data('content');

        $(this).manage_pkl_content(content);
    });

    $(this).on('click', '.btn-detail-pkl', function() {
        var id = $(this).data('id');
        var source = $(this).data('source');
        $.ajax({
            url: site_url + 'async_admin/pkl_detail_modal',
            type: 'POST',
            data: {
                pkl_id: id,
                source: source
            },
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-detail-modal').modal('show');
                $('#pkl-detail-modal').html(r);
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
    });

    $(this).on('click', '.btn-delete-pkl', function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var content = $(this).data('content');
        $.ajax({
            url: site_url + 'async_admin/pkl_delete_modal',
            type: 'POST',
            data: {
                pkl_id: id,
                name: name,
                content: content
            },
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-delete-modal').modal('show');
                $('#pkl-delete-modal').html(r);
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
    });

    $(this).on('submit', '#form-delete-pkl', function(e) {
        var content = $(this).data('content');
        $(this).ajaxSubmit({
            url: site_url + 'async_admin/delete_pkl',
            type: 'POST',
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-delete-modal').modal('hide');
                if(r.status) {
                    $(this).notification('success', r.message);
                } else {
                    $(this).notification('error', r.message);
                }
                $(this).manage_pkl_content(content);
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
        e.preventDefault();
    });

    $(this).on('click', '.btn-block-pkl', function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $.ajax({
            url: site_url + 'async_admin/pkl_block_modal',
            type: 'POST',
            data: {
                pkl_id: id,
                name: name
            },
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-block-modal').modal('show');
                $('#pkl-block-modal').html(r);
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
    });

    $(this).on('submit', '#form-block-pkl', function(e) {
        var id = $(this).find('input[name="pkl_id"]').val();
        $(this).request_action(id, 2, 'pkl_accepted');
        e.preventDefault();
    });

    $(this).on('click', '.btn-edit-pkl', function() {
        var id = $(this).data('id');
        $.ajax({
            url: site_url + 'async_admin/pkl_edit_modal',
            type: 'POST',
            data: {
                pkl_id: id
            },
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-edit-modal').modal('show');
                $('#pkl-edit-modal').html(r);
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
    });

    $(this).on('submit', '#form-edit-pkl', function(e) {
        $(this).ajaxSubmit({
            url: site_url + 'async_admin/edit_pkl',
            type: 'POST',
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-edit-modal').modal('hide');
                if(r.status) {
                    $(this).notification('success', r.message);
                } else {
                    $(this).notification('error', r.message);
                }
                $(this).manage_pkl_content('pkl_accepted');
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
        e.preventDefault();
    });

    $(this).on('click', '.btn-accept-pkl', function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $.ajax({
            url: site_url + 'async_admin/pkl_accept_modal',
            type: 'POST',
            data: {
                pkl_id: id,
                name: name
            },
            beforeSend: function() {
                NProgress.start();
            },
            success: function(r) {
                NProgress.done();
                $('#pkl-accept-modal').modal('show');
                $('#pkl-accept-modal').html(r);
            },
            error: function() {
                NProgress.done();
                $(this).notification('error', 'Connection Time Out');
            }
        });
    });

    $(this).on('submit', '#form-accept-pkl', function(e) {
        var id = $(this).find('input[name="pkl_id"]').val();
        $(this).request_action(id, 1, 'pkl_rejected');
        e.preventDefault();
    });
});
