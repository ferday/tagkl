$(function() {

	$(this).on('submit', '#signin-form', function(e) {
		$('#signin-form').ajaxSubmit({
			url: site_url + 'auth/login',
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				NProgress.start();
			},
			success: function(r) {
				if(r.status) {
					window.location.href = r.target;
				} else {
					$(this).notification('error', 'Nama pengguna atau kata sandi salah');
					$('#signin-form input[type="password"]').val('');
				}
				NProgress.done();
			},
			error: function() {
				NProgress.done();
			}
		});
		e.preventDefault();
	});

	$(this).on('click', '.signup-btn', function() {
		var type = $(this).data('type');

		$('#signup-modal-'+type).modal('show');

		// $.ajax({
		// 	url: site_url + 'auth/signup_modal/'+type,
		// 	type: 'GET',
		// 	beforeSend: function() {
		// 		NProgress.start();
		// 	},
		// 	success: function(r) {
		// 		NProgress.done();
		// 		$("#signup-modal").html(r);
		// 		$("#signup-modal").modal('show');
		// 	},
		// 	error: function() {
		// 		NProgress.done();
		// 	}
		// });
	});

	$(this).on('submit', '#register', function(e) {
		$('#register').ajaxSubmit({
			url: site_url + 'auth/register',
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				$('.register-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Loading...');
			},
			success: function(r) {
				$('.register-btn').html('Daftar Gratis');
				if(r.status) {
					$(this).notification('success', r.message);
					var activate_body = '<h3 class="text-center">Tinggal satu langkah lagi !</h3>';
						activate_body += '<p class="text-center">Silahkan cek email untuk mengaktifkan akun TagKL mu.</p>';
					$('#signup-modal-pembeli .modal-body').html(activate_body);
				} else {
					$(this).notification('error', r.message);
					$('input[name="'+r.target+'"]').parent().addClass('has-error');
				}
			},
			error: function() {
				$('.register-btn').html('Daftar Gratis');
				$(this).notification('error', 'Unknown error occured');
			}
		});
		e.preventDefault();
	});

	$(this).on('submit', '#pkl-register', function(e) {
		$(this).ajaxSubmit({
			url: site_url + 'auth/pkl_register',
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				$('.register-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Loading...');
			},
			success: function(r) {
				$('.register-btn').html('Daftar Gratis');
				if(r.status) {
					$(this).notification('success', r.message);
					var activate_body = '<h3 class="text-center">Tinggal satu langkah lagi !</h3>';
					activate_body += '<p class="text-center">Kami akan menghubungi anda melalui email untuk mulai menggunakan TagKL untuk PKL.</p>';
					$('#signup-modal-pkl .modal-body').html(activate_body);
				} else {
					$(this).notification('error', r.message);
					$('input[name="'+r.target+'"]').parent().addClass('has-error');
				}
			},
			error: function() {
				$('.register-btn').html('Daftar Gratis');
				$(this).notification('error', 'Unknown error occured');
			}
		});
		e.preventDefault();
	});

	$(this).on('change', '.upload', function(e) {
		$('.fileUploadName').html(e.target.files[0].name);
		e.preventDefault();
	});
});